
/**
 * Module dependencies.
 */

var express = require('express')
/*   , routes = require('./routes') */
/*   , user = require('./routes/user') */
  , http		= require('http')
  , path		= require('path')
  , mongoose 	= require('mongoose')
  , User		= require('./models/user.js')
  , Country		= require('./models/country.js')
  , graph		= require("fbgraph")
  , conf		= require("./config");
  
mongoose.connect(conf.db_url);





var app = express();




/*
 * Configure Express
 *
 */


app.configure(function(){
  app.set('port', process.env.PORT || 3333);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser({ keepExtensions: true, uploadDir: __dirname + "/public/uploads" }));
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});



var Level1 = mongoose.model("level1", {
	
	flags: [String],
	correct: String
});

var Level2 = mongoose.model("level2", {
	choices: [String],
	correct: String,
	correctIndex: Number
});

var Level3 = mongoose.model("level3", {
	name: String,
	x: Number,
	y: Number
});


















/*
 * Facebook auth
 *
 */

function facebook_auth(req, res) {
	
	// we don't have a code yet
	// so we'll redirect to the oauth dialog
	if (!req.query.code) {
		var authUrl = graph.getOauthUrl({
			"client_id":		conf.client_id
			, "redirect_uri":	conf.redirect_uri
			, "scope":			conf.scope
		});
		
		if (!req.query.error) { //checks whether a user denied the app facebook login/permissions
			res.redirect(authUrl);
		} else {  //req.query.error == 'access_denied'
			render_page (req, res);
			res.send('access denied');
		}
		return;
	}
	
	if (graph.getAccessToken() === null) {
	
		// code is set
		// we'll send that and get the access token
		graph.authorize({
			"client_id":		conf.client_id
			, "redirect_uri":	conf.redirect_uri
			, "client_secret":	conf.client_secret
			, "code":			req.query.code
		}, function (err, facebookRes) {
			console.log(facebookRes);
			render_page (req, res);
		});
	} else {
		render_page (req, res);
	}
	render_page (req, res);
}


function render_page (req, res) {
	
	graph.get('/me', function (err, res) {
		console.log(res);
	});
	
	Level1.find({}, function (err, l1) {
		Level2.find({}, function (err, l2) {
			Level3.find({}, function (err, l3) {	
				res.render('index', {
					title: 'World Challenge',
					conf: conf,
					graph: graph,
					level1: l1,
					level2: l2,
					level3: l3
				});
			});
		});
	});
}











/*
 * Routes
 *
 */


app.get('/', function (req, res) {
	
	
	Level1.find({}, function (err, l1) {
		Level2.find({}, function (err, l2) {
			Level3.find({}, function (err, l3) {	
				res.render('index', {
					title: 'World Challenge',
					conf: conf,
					graph: graph,
					level1: l1,
					level2: l2,
					level3: l3
				});
			});
		});
	});
});

app.post('/', facebook_auth);

app.get('/canvas', facebook_auth);
app.post('/canvas', facebook_auth);



app.get('/countries', function (req, res) {
	
	Country.find({}, function (err, countries) {
		
		if (err) throw err;
		
		res.render('countries', {
			title: 'Paises',
			conf: conf,
			graph: graph,
			countries: countries
		});
		
	});
	
	
});

app.get('/country/:id', function (req, res) {
	
	Country.findById(req.params.id, function (err, country) {
		
		if (err) throw err;
		
		res.render('country', {
			title: country.nombre,
			conf: conf,
			graph: graph,
			country: country
		});
		
	});
	
	
});

app.post('/country', function (req, res) {
	
	var country = new Country(req.body);
	
	console.log(req.body);
	
	/*country.save( function (err) {
		
		if (err) throw err;
		
		res.send('OK');
	})*/
	res.send('OK');
});

app.post('/user', function (req, res) {

	var user = new User({
		username: req.body.username,
		highscores: [req.body.highscore],
		maxHighscore: req.body.highscore
	});

	user.save( function (err) {

		if (err) throw err;

		res.send(user);
	});

});


app.post('/user/:id/addHighscore', function (req, res) {

	User
		.findOne({username:req.params.id}, function (err, user) {

			user.highscores.sort();

			var pushable = {
				$push: {
					highscores: req.body.highscore
				}
			};

			if (user.highscores[0] < req.body.highscore) {

				pushable["maxHighscore"] = req.body.highscore;

			}
			
			console.log(pushable);
			
			User
				.findByIdAndUpdate(
					req.params.id,
					pushable,
					function (err, u) {

						if (err) throw err;

						res.send(u);
				});
	});
});


app.get('/highscores', function (req, res) {


	User
		.find()
		.sort({
			maxHighscore: -1
		})
		.limit(10)
		.select('username maxHighscore')
		.exec( function (err, users) {

			res.send(users);
		})
});







/*
 * Socket.IO
 *
 */
 
var server = app.listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
});


var io = require('socket.io').listen(server);
io.set('loglevel',5) // set log level to get all debug messages

io.sockets.on('connection', function (client){
    
    client.on('message', function (data) {
    	
    	console.log(data);
    	
    	User
    		.findOne()
    		.sort({
    			maxHighscore: -1
    		})
    		.where('maxHighscore').lt(data)
    		.exec(function (err, user) {
    			if (user)
		    		client.emit('message',user);
    		});
    });
    
    client.on('disconnect', function () {
    
    });
});