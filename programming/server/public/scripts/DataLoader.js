var DataLoader = function (assets, flag, sound, updateLoadingNumber, updateLoadingText, didEnd, obelisk) {
	
	var	  size  = 0
		, progress = 0
		, didFinishFS = false
		, didFinishLocal = false
		, data = {
			'assets': assets,
			'flags': flag,
			'sounds': sound,
			'monuments': obelisk
		};
	
	size += Object.size(assets);
	size += Object.size(flag);
	size += Object.size(sound);
	size += Object.size(obelisk);
	size += 24;
	
	function getProgress() {
		return Math.round(progress * 100 / size);
	}
	
	(function () {
		
		function fetchFSImages() {
		
			var j = 0;
			var l = 0;
			var skipIndex = 0;
			var venueIndex = 0;
			var venueIndexArray = new Array(10);
			var willLoad = Math.floor(Object.size(fs_pics)/4);
			
			console.log(willLoad);
			
			function pushToLevel() {
						
				level5Answers.push({
					options: [fs_pics[venueIndex]['venue']['name']],
					i: null,
					correctIndex: skipIndex
				});
				
				if (skipIndex != 0) {
					level5Answers[Math.floor(j/4)].options.push(fs_pics[venue]['venue']['name']);
				}
				
				near[fs_pics[venueIndex]['venue']['name']] = new Image();
				
				near[fs_pics[venueIndex]['venue']['name']].onload = function() {
					
					
					level5Answers[l].i = near[fs_pics[venueIndexArray[l]]['venue']['name']];
					progress++;
					l++;
					updateLoadingNumber(getProgress());
					if (l == willLoad) {
						console.log('finished loading 4sq');
						updateLoadingText('Carga de imagenes de lugares cercanos completa');
						didFinishFS = true;
						if (didFinishLocal) {
							didEnd();
						}
/* 						initStage(); */
					}
				}

				
				near[fs_pics[venueIndex]['venue']['name']].src = fs_pics[venueIndex]['venue']['photos']['groups'][0]['items'][0]['url'];
			}
			
			for (venue in fs_pics) {
				
				if (j % 4 == 0) {
					
					var didGetResult = false;
					
					while (!didGetResult) {
						
						skipIndex = parseInt((Math.random() * 100) % 4);
						venueIndex = parseInt(venue) + skipIndex;
						venueIndexArray[Math.floor(j/4)] = venueIndex;
						if (fs_pics[venueIndex]['venue']['photos']['groups'][0]) {
							didGetResult = true;
							console.log(fs_pics[venueIndex]['venue']['photos']['groups'][0]['items'][0]['url']);
							pushToLevel();
						}	
					}
					
					
				} else {
					
					if (j != venueIndex) {
						
						level5Answers[Math.floor(j/4)].options.push(fs_pics[venue]['venue']['name']);	
					}		
				}
				
				j++;
			}
			
			console.log(level5Answers);
		}
		
		function success(position) {
			progress++;
			
			updateLoadingText('Busqueda de GPS exitosa');
			console.log(position);
			updateLoadingText('Buscando lugares cercanos');
			
			$.ajax({
			
				url: 'https://api.foursquare.com/v2/venues/explore?venuePhotos=1&ll=' + position.coords.latitude + ',' + position.coords.longitude + fs_auth + '&section=shopping&query=art&limit=40',
				success: function (data) {
					
					updateLoadingText('Busqueda de lugares cercanos exitosa');
					
					fs_pics = data['response']['groups'][0]['items'];
/* 					size += fs_pics.length; */
					progress++;
					updateLoadingNumber(getProgress());

					console.log(data);

					fetchFSImages();
					
					
				},
				error: function () {
					updateLoadingText('Busqueda de lugares cercanos fallo');
				}
			
			});
			
		}
		
		function error(msg) {
			updateLoadingText('Busqueda de GPS fallo');
			alert(msg);
			console.log(msg);
			didEnd();
		}
		
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(success, error);
		} else {
			error('not supported');
		}
		
		
	})();
	
	
	function getIntoMemory(root, source, destination, completeMessage) {
		
	    var loadedImages = 0;
	    var numImages = Object.size(source);
	    for (var src in source) {
	    	destination[src] = new Image();
	    	destination[src].onload = function() {
	    		progress++;
	    		updateLoadingNumber(getProgress());
	    		
	    		if(++loadedImages >= numImages) {
	    			updateLoadingText(completeMessage);
	    			return;
	    		}
	    	};
	    	
/* 	    	if (source[src].substr(0,4) == 'http') { */
/* 		    	destination[src].src = source[src]; */
/* 	    	} */
/* 		    else { */
		    	destination[src].src = root + source[src];
/* 		    } */
	    }
		
	}
	
	return {
		
		load: function () {
			
			// Main assets
			getIntoMemory('../assets/', data.assets, images, "Imagenes de programa cargadas");
			// Flags
			getIntoMemory('../assets/flags/', data.flags, flags, "Banderas cargadas");
			// Monuments
			getIntoMemory('../assets/monumentos_ok/', data.monuments, monuments, "Monumentos cargados");
			
			var root = '../assets/sounds/';
		    var loadedImages = 0;
		    var numImages = Object.size(data.sounds);
		    for (var src in sourceSounds) {
		    	sounds[src] = new Audio();
		    	sounds[src].src = root + data.sounds[src];
		    	progress++;
		    	updateLoadingNumber(getProgress());
		    }
		    console.log("loaded sounds");
			
			sounds.tick.volume = 0.5;

			didFinishLocal = true;
			if (didFinishFS) {
				didEnd();
			}
			
		}
	}
	
}