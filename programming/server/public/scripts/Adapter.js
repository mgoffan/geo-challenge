var Adapter = (function () {
	
	return {
		
		convertCoords: function (coords, mapW, mapH) {
/* 			console.log([mapW,mapH]); */
			
			for (var i = 0; i < coords.length; i++) {
				
				var points = coords[i][0];
				
				for (var j = 0; j < points.length; j++) {
				
					points[j][0] *= mapW / 360;
					points[j][1] *= -mapH / 180;
				}
			}
			return coords;
		},
		
		convertPoints: function (points, mapW, mapH) {
		
			points[0] *= mapW / 360;
			points[1] *= -mapH / 180;
			
			return points
		}
	}
	
})(Adapter || {});