var ANIMATIONS = {
	
	LOAD: {
		
		FONDO: {'idle':[{"y": 604, "x": 1206, "width": 600, "height": 600}, {"y": 1206, "x": 2, "width": 600, "height": 600}, {"y": 2, "x": 604, "width": 600, "height": 600}, {"y": 2, "x": 1206, "width": 600, "height": 600}, {"y": 604, "x": 604, "width": 600, "height": 600}, {"y": 1206, "x": 604, "width": 600, "height": 600}, {"y": 604, "x": 2, "width": 600, "height": 600}, {"y": 2, "x": 2, "width": 600, "height": 600}, {"y": 604, "x": 2, "width": 600, "height": 600}, {"y": 1206, "x": 604, "width": 600, "height": 600}, {"y": 604, "x": 604, "width": 600, "height": 600}, {"y": 2, "x": 1206, "width": 600, "height": 600}, {"y": 2, "x": 604, "width": 600, "height": 600}, {"y": 1206, "x": 2, "width": 600, "height": 600}]},
		MUNDO: {"idle":[{"y": 254, "x": 1010, "height": 250, "width": 250}, {"y": 2, "x": 1010, "height": 250, "width": 250}, {"y": 254, "x": 758, "height": 250, "width": 250}, {"y": 2, "x": 758, "height": 250, "width": 250}, {"y": 254, "x": 506, "height": 250, "width": 250}, {"y": 2, "x": 506, "height": 250, "width": 250}, {"y": 254, "x": 254, "height": 250, "width": 250}, {"y": 2, "x": 254, "height": 250, "width": 250}, {"y": 254, "x": 2, "height": 250, "width": 250}, {"y": 2, "x": 2, "height": 250, "width": 250}]}
	},
	
	MENU: {
		
	},
	
	STAGE1: {
		
		YETI : {}
	},
	
	STAGE2: {
		
		YETI : {'hooray': [{"y": 2, "x": 2, "width": 160, "height": 180}, {"y": 2, "x": 164, "width": 160, "height": 180}, {"y": 2, "x": 326, "width": 160, "height": 180}, {"y": 2, "x": 488, "width": 160, "height": 180}, {"y": 2, "x": 650, "width": 160, "height": 180}, {"y": 2, "x": 812, "width": 160, "height": 180}, {"y": 2, "x": 974, "width": 160, "height": 180}, {"y": 2, "x": 1136, "width": 160, "height": 180}, {"y": 2, "x": 1298, "width": 160, "height": 180}, {"y": 2, "x": 2, "width": 160, "height": 180}, {"y": 2, "x": 1460, "width": 160, "height": 180}, {"y": 2, "x": 1622, "width": 160, "height": 180}, {"y": 2, "x": 1784, "width": 160, "height": 180}, {"y": 184, "x": 2, "width": 160, "height": 180}, {"y": 184, "x": 164, "width": 160, "height": 180}, {"y": 184, "x": 326, "width": 160, "height": 180}, {"y": 184, "x": 488, "width": 160, "height": 180}, {"y": 184, "x": 650, "width": 160, "height": 180}, {"y": 184, "x": 812, "width": 160, "height": 180}, {"y": 2, "x": 2, "width": 160, "height": 180}], 'idle': [{"y": 2, "x": 2, "width": 160, "height": 180}, {"y": 184, "x": 974, "width": 160, "height": 180}, {"y": 184, "x": 1136, "width": 160, "height": 180}, {"y": 184, "x": 1298, "width": 160, "height": 180}, {"y": 184, "x": 1460, "width": 160, "height": 180}, {"y": 184, "x": 1622, "width": 160, "height": 180}, {"y": 184, "x": 1784, "width": 160, "height": 180}, {"y": 366, "x": 2, "width": 160, "height": 180}, {"y": 366, "x": 164, "width": 160, "height": 180}, {"y": 366, "x": 326, "width": 160, "height": 180}, {"y": 366, "x": 488, "width": 160, "height": 180}, {"y": 366, "x": 650, "width": 160, "height": 180}, {"y": 366, "x": 812, "width": 160, "height": 180}, {"y": 366, "x": 974, "width": 160, "height": 180}, {"y": 366, "x": 1136, "width": 160, "height": 180}, {"y": 366, "x": 1298, "width": 160, "height": 180}, {"y": 366, "x": 1460, "width": 160, "height": 180}, {"y": 366, "x": 1622, "width": 160, "height": 180}, {"y": 366, "x": 1784, "width": 160, "height": 180}, {"y": 2, "x": 2, "width": 160, "height": 180}]}
	},
	
	STAGE3: {
		
		YETI : {
			
		}
	},
	
	STAGE4: {
		
		YETI : {'hooray': [{"y": 2, "x": 2, "width": 160, "height": 180}, {"y": 184, "x": 1460, "width": 160, "height": 180}, {"y": 366, "x": 2, "width": 160, "height": 180}, {"y": 366, "x": 164, "width": 160, "height": 180}, {"y": 366, "x": 326, "width": 160, "height": 180}, {"y": 366, "x": 488, "width": 160, "height": 180}, {"y": 366, "x": 650, "width": 160, "height": 180}, {"y": 366, "x": 812, "width": 160, "height": 180}, {"y": 366, "x": 974, "width": 160, "height": 180}, {"y": 366, "x": 1136, "width": 160, "height": 180}, {"y": 366, "x": 1298, "width": 160, "height": 180}, {"y": 366, "x": 1460, "width": 160, "height": 180}, {"y": 548, "x": 2, "width": 160, "height": 180}, {"y": 548, "x": 164, "width": 160, "height": 180}, {"y": 548, "x": 326, "width": 160, "height": 180}, {"y": 548, "x": 488, "width": 160, "height": 180}, {"y": 548, "x": 650, "width": 160, "height": 180}, {"y": 548, "x": 812, "width": 160, "height": 180}, {"y": 548, "x": 974, "width": 160, "height": 180}, {"y": 2, "x": 2, "width": 160, "height": 180}], 'idle': [{"y": 2, "x": 2, "width": 160, "height": 180}, {"y": 2, "x": 164, "width": 160, "height": 180}, {"y": 2, "x": 326, "width": 160, "height": 180}, {"y": 2, "x": 488, "width": 160, "height": 180}, {"y": 2, "x": 650, "width": 160, "height": 180}, {"y": 2, "x": 812, "width": 160, "height": 180}, {"y": 2, "x": 974, "width": 160, "height": 180}, {"y": 2, "x": 1136, "width": 160, "height": 180}, {"y": 2, "x": 1298, "width": 160, "height": 180}, {"y": 2, "x": 1460, "width": 160, "height": 180}, {"y": 184, "x": 2, "width": 160, "height": 180}, {"y": 184, "x": 164, "width": 160, "height": 180}, {"y": 184, "x": 326, "width": 160, "height": 180}, {"y": 184, "x": 488, "width": 160, "height": 180}, {"y": 184, "x": 650, "width": 160, "height": 180}, {"y": 184, "x": 812, "width": 160, "height": 180}, {"y": 184, "x": 974, "width": 160, "height": 180}, {"y": 184, "x": 1136, "width": 160, "height": 180}, {"y": 184, "x": 1298, "width": 160, "height": 180}, {"y": 2, "x": 2, "width": 160, "height": 180}]}
	},
	
	STAGE5: {
		
		YETI : {'hooray': [{"y": 1034, "x": 2, "width": 332, "height": 400}, {"y": 1034, "x": 336, "width": 332, "height": 400}, {"y": 1034, "x": 670, "width": 332, "height": 400}, {"y": 1034, "x": 1004, "width": 330, "height": 400}, {"y": 1034, "x": 1336, "width": 329, "height": 400}, {"y": 1034, "x": 1667, "width": 328, "height": 400}, {"y": 1436, "x": 2, "width": 326, "height": 400}, {"y": 1436, "x": 330, "width": 323, "height": 400}, {"y": 1436, "x": 655, "width": 320, "height": 400}, {"y": 1436, "x": 977, "width": 318, "height": 400}, {"y": 1436, "x": 1297, "width": 320, "height": 400}, {"y": 1436, "x": 1619, "width": 324, "height": 400}, {"y": 1436, "x": 1945, "width": 327, "height": 400}, {"y": 1838, "x": 2, "width": 328, "height": 400}, {"y": 1838, "x": 332, "width": 330, "height": 400}, {"y": 1838, "x": 664, "width": 332, "height": 400}, {"y": 1838, "x": 998, "width": 332, "height": 400}, {"y": 1838, "x": 1332, "width": 334, "height": 400}, {"y": 1838, "x": 1668, "width": 335, "height": 400}, {"y": 1034, "x": 2, "width": 332, "height": 400}], 'idle': [{"y": 2, "x": 2, "width": 341, "height": 342}, {"y": 2, "x": 345, "width": 341, "height": 342}, {"y": 2, "x": 688, "width": 340, "height": 342}, {"y": 2, "x": 1030, "width": 338, "height": 342}, {"y": 2, "x": 1370, "width": 336, "height": 342}, {"y": 2, "x": 1708, "width": 333, "height": 342}, {"y": 346, "x": 2, "width": 332, "height": 342}, {"y": 346, "x": 336, "width": 328, "height": 342}, {"y": 346, "x": 666, "width": 325, "height": 342}, {"y": 346, "x": 993, "width": 321, "height": 342}, {"y": 346, "x": 1316, "width": 325, "height": 342}, {"y": 346, "x": 1643, "width": 327, "height": 342}, {"y": 346, "x": 1972, "width": 331, "height": 342}, {"y": 690, "x": 2, "width": 333, "height": 342}, {"y": 690, "x": 337, "width": 335, "height": 342}, {"y": 690, "x": 674, "width": 336, "height": 342}, {"y": 690, "x": 1012, "width": 338, "height": 342}, {"y": 690, "x": 1352, "width": 340, "height": 342}, {"y": 690, "x": 1694, "width": 340, "height": 342}, {"y": 2, "x": 2, "width": 341, "height": 342}]},
	},
	
};