/*
 * game.js
 * works
 */

var Game = (function () {
	
	var LEVEL = {
		RANKINGS	: -2,
		OPTIONS		: -1,
		MAIN		: 0,
		LEVEL1		: 1,
		LEVEL2		: 2,
		LEVEL3		: 3,
		LEVEL4		: 4,
		LEVEL5		: 5,
		SCORE		: 10,
		CREDITS		: 11,
		
		LEVEL1LOADER:	50,
		LEVEL2LOADER:	51,
		LEVEL3LOADER:	52,
		LEVEL4LOADER:	53,
		LEVEL5LOADER:	54,
		
		LEVELLOADER:	55
	};
	
	var   currentLevel = 0
		, target = 0;
	
	function gotoNextLevel (destination) {
		currentLevel = destination;
		
		switch (destination) {
			case LEVEL.RANKINGS:
				Game.gotoScores();
				break;
			case LEVEL.OPTIONS:
				Game.gotoOptions();
				break;
			case LEVEL.MAIN:
				Game.gotoMain();
				break;
			case LEVEL.LEVEL1:
				Game.gotoLevel1();
				break;
			case LEVEL.LEVEL2:
				Game.gotoLevel2();
				break;
			case LEVEL.LEVEL3:
				Game.gotoLevel3();
				break;
			case LEVEL.LEVEL4:
				Game.gotoLevel4();
				break;
			case LEVEL.LEVEL5:
				Game.gotoLevel5();
				break;
			case LEVEL.SCORE:
				Game.gotoResults();
				break;
			case LEVEL.CREDITS:
				Game.gotoCredits();
				break;
			case LEVEL.LEVEL1LOADER:
				Game.gotoLevel1Loader();
				break;
			case LEVEL.LEVEL2LOADER:
				Game.gotoLevel2Loader();
				break;
			case LEVEL.LEVEL3LOADER:
				Game.gotoLevel3Loader();
				break;
			case LEVEL.LEVEL4LOADER:
				Game.gotoLevel4Loader();
				break;
			case LEVEL.LEVEL5LOADER:
				Game.gotoLevel5Loader();
				break;
			case LEVEL.LEVELLOADER:
				Game.gotoLevelLoader(target);
				break;
		}
	}

	function transitionFadeIn() {
		var tween = new Kinetic.Tween({
			node:		stage,
			duration: 	0.75,
			easing:		Kinetic.Easings.EaseIn,
			opacity:	1,
			onFinish:	function () {
				
				//stage.clear();
				//gotoNextLevel(destination);
			}
		});
		
		tween.play();
	}
	
	function transitionFadeOut(destination) {
	
		var tween = new Kinetic.Tween({
			node:		stage,
			duration: 	1.5,
			easing:		Kinetic.Easings.EaseIn,
			opacity:	0,
			onFinish:	function () {
			
				backgroundLayer.removeChildren();
				shapesLayer.removeChildren();
/* 				imagesLayer.removeChildren(); */
/* 				alternativeLayer.removeChildren(); */
				loadingLayer.removeChildren();
				polysLayer.removeChildren();
				
				gotoNextLevel(destination);
				transitionFadeIn();
			}
		});
		
		tween.play();
	}
	
	return {
	
		transitionFadeOut: function(destination) {
	
			var tween = new Kinetic.Tween({
				node:		stage,
				duration: 	1.5,
				easing:		Kinetic.Easings.EaseIn,
				opacity:	0,
				onFinish:	function () {
				
					backgroundLayer.destroyChildren();
					shapesLayer.destroyChildren();
/* 					imagesLayer.destroyChildren(); */
/* 					alternativeLayer.destroyChildren(); */
					loadingLayer.destroyChildren();
					polysLayer.destroyChildren();
					
					gotoNextLevel(destination);
					transitionFadeIn();
				}
			});
			
			tween.play();
		},
	
	
		gotoMain: function () {
			
			if (sound_on)
				song.play();
			
		    var bg = new Kinetic.Image({
			   image: images.bg,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight
		    });
		    
		    var jugar = new Kinetic.Image({
		        image: images.jugar,
		        width: images.jugar.width * 0.5 * screenWidthFactor,
		        height: images.jugar.height * 0.5 * screenHeightFactor,
/* 		        x: , */
		        y: screenHeight / 2 - 0 * screenHeightFactor,
		    });
		    
		    jugar.setX(screenWidth / 2 - jugar.getWidth() / 2 + 0);
		    
		    var opciones = new Kinetic.Image({
		        image: images.opciones,
		        width: images.opciones.width * 0.5 * screenWidthFactor,
		        height: images.opciones.height * 0.5 * screenHeightFactor,
/* 		        x: screenWidth / 2 - images.opciones.width / 2 + 0, */
		        y: jugar.getY() + jugar.getHeight() + 10 * screenHeightFactor
		    });
		    
		    opciones.setX(screenWidth / 2 - opciones.getWidth() / 2 + 0);
		    
		    var rankings = new Kinetic.Image({
		        image: images.rankings,
		        width: images.rankings.width * 0.5 * screenWidthFactor,
		        height: images.rankings.height * 0.5 * screenHeightFactor,
/* 		        x: screenWidth / 2 - images.rankings.width / 2 + 0, */
		        y: opciones.getY() + opciones.getHeight() + 10 * screenHeightFactor
		    });
		    
		    rankings.setX(screenWidth / 2 - rankings.getWidth() / 2 + 0)
		    
		    var credits = new Kinetic.Image({
		        image: images.credits,
		        width: images.credits.width * 0.5 * screenWidthFactor,
		        height: images.credits.height * 0.5 * screenHeightFactor,
/* 		        x: screenWidth / 2, //- credits.getWidth() / 2 + 0, */
		        y: rankings.getY() + rankings.getHeight() + 10 * screenHeightFactor
		    });
		    
		    credits.setX(screenWidth / 2 - credits.getWidth() / 2 + 0)
		    
		    var cloud = new Kinetic.Image({
			    image: images.cloud,
			    x : -20,
			    y: screenHeight / 2,
			    opacity: .7
		    });
		    
		    var cloud2 = new Kinetic.Image({
			    image: images.cloud,
			    x : -20,
			    y: screenHeight / 2,
			    opacity: .7
		    });
		    
		    var cloud3 = new Kinetic.Image({
			    image: images.cloud,
			    x : -20,
			    y: screenHeight / 2,
			    opacity: .7
		    });
		    
		    var cloud4 = new Kinetic.Image({
			    image: images.cloud,
			    x : -20,
			    y: screenHeight / 2,
			    opacity: .7
		    });
		    
		    var cloud5 = new Kinetic.Image({
			    image: images.cloud,
			    x : -20,
			    y: screenHeight / 2,
			    opacity: .7
		    });
		    
		    var cloud6 = new Kinetic.Image({
			    image: images.cloud,
			    x : -20,
			    y: screenHeight / 2,
			    opacity: .7
		    });
		    
		    
		    /**
		     *
		     *  Add images to layers
		     *  and layers to the stage
		     */
		    
		    backgroundLayer.add(bg);
		    
		    shapesLayer.add(cloud);
		    shapesLayer.add(cloud2);
		    shapesLayer.add(cloud3);
		    shapesLayer.add(cloud4);
		    shapesLayer.add(cloud5);
		    shapesLayer.add(cloud6);
		    
		    shapesLayer.add(jugar);
		    shapesLayer.add(opciones);
		    shapesLayer.add(rankings);
		    shapesLayer.add(credits);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
		    /**
		     *
		     *  Set Animations
		     */
		    
		    var shouldBounce = false; // Animation Toggle
		    var   amplitude = 0.02
		    	, base = 0.6
		    	, frequency = 0.004;
		    
		    var menuAnim = new Kinetic.Animation( function (frame) {
				
				if (shouldBounce) {
					jugar
						.setScale( Math.sin( frame.time * frequency)                * amplitude + base);
					opciones
						.setScale( Math.sin( frame.time * frequency + Math.PI / 2)  * amplitude + base);
					rankings
						.setScale( Math.sin( frame.time * frequency)                * amplitude + base);
				}
				
				cloud.setScale( Math.sin( frame.time * frequency) * amplitude + base - 0.1);
				cloud.setX( Math.cos(frame.time * Math.PI / 15000 * 2) * screenWidth / 2 + screenWidth / 2);
				cloud.setY( -Math.sin(frame.time * Math.PI / 1250) * screenHeightFactor * 5 + screenHeight / 2);
				
				cloud2.setScale( Math.sin( frame.time * frequency) * amplitude + base - 0.1);
				cloud2.setX( -Math.cos(frame.time * Math.PI / 15000 * 1.2) * screenWidth / 2 + screenWidth / 2);
				cloud2.setY( Math.sin(frame.time * Math.PI / 1250) * screenHeightFactor * 7.5 + screenHeight / 2);
				
				cloud3.setScale( Math.sin( frame.time * frequency) * amplitude + base - 0.1);
				cloud3.setX( Math.sin(frame.time * Math.PI / 15000 + screenWidth / 2) * screenWidth / 2 + screenWidth / 2);
				cloud3.setY( Math.sin(frame.time * Math.PI / 1250) * screenHeightFactor * 20 + screenHeight / 2);
				
				cloud4.setScale( Math.sin( frame.time * frequency) * amplitude + base - 0.1);
				cloud4.setX( -Math.sin(frame.time * Math.PI / 15000 + screenWidth / 3) * screenWidth / 2 + screenWidth / 2);
				cloud4.setY( -Math.sin(frame.time * Math.PI / 1250) * screenHeightFactor * 15 + screenHeight / 2);
				
				cloud5.setScale( Math.sin( frame.time * frequency) * amplitude + base - 0.1);
				cloud5.setX( Math.cos(frame.time * Math.PI / 15000 + screenWidth / 6) * screenWidth / 2 + screenWidth / 2);
				cloud5.setY( Math.sin(frame.time * Math.PI / 1250) * screenHeightFactor * 10 + screenHeight / 2);
				
				cloud6.setScale( Math.sin( frame.time * frequency) * amplitude + base - 0.1);
				cloud6.setX( -Math.cos(frame.time * Math.PI / 15000 + screenWidth / 8) * screenWidth / 2 + screenWidth / 2);
				cloud6.setY( -Math.sin(frame.time * Math.PI / 1250) * screenHeightFactor * 25 + screenHeight / 2);
				
		
			}, shapesLayer);
			
			menuAnim.start();
			
			
			/**
			 *
			 *
			 *  Listen for events
			 *
			 */
			 
			 
				 /**
				  *
				  *  Jugar bindings
				  */
					jugar.on('mouseover touchstart', function () {
						jugar.setImage(images.jugar_alt);
					});
					
					jugar.on('mouseout touchstart', function () {
						jugar.setImage(images.jugar);
					});
					
					jugar.on('mousedown touchstart', function () {
						target = LEVEL.LEVEL1;
						transitionFadeOut(LEVEL.LEVELLOADER);
					});
			
				/**
				  *
				  *  Opciones bindings
				  */
			
					opciones.on('mouseover touchend', function () {
						opciones.setImage(images.opciones_alt);
					});
					
					opciones.on('mouseout touchend', function () {
						opciones.setImage(images.opciones);
					});
					
					opciones.on('mousedown touchstart', function () {
						transitionFadeOut(LEVEL.OPTIONS);
					});
					
				/**
				  *
				  *  Rankings bindings
				  */
			
					rankings.on('mouseover touchstart', function () {
						rankings.setImage(images.rankings_alt);
					});
					
					rankings.on('mouseout touchend', function () {
						rankings.setImage(images.rankings);
					});
					
					rankings.on('mousedown touchstart', function () {
						transitionFadeOut(LEVEL.RANKINGS);
					});
				
				/**
				  *
				  *  Credits bindings
				  */
			
					credits.on('mouseover touchstart', function () {
						credits.setImage(images.credits_alt);
					});
					
					credits.on('mouseout touchend', function () {
						credits.setImage(images.credits);
					});
					
					credits.on('mousedown touchstart', function () {
						transitionFadeOut(LEVEL.CREDITS);
					});
		},
		
		/*
		 *
		 * END of Main
		 *
		 * Beginning of Level 1
		 *
		 * El de las banderas
		 *
		 */
		
		gotoLevel1: function () {
		
			console.log("Level 1");
		
			shapesLayer.add(winPlaceholder);
			shapesLayer.add(winText);
		
		    var bg = new Kinetic.Image({
			   image: images.etapa1,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight
		    });
		    
		    var clock = new Kinetic.Image({
		    	image: images.clock,
		    	x: screenWidth - 160,
		    	y: 20
		    });
		    
		    var timerText = new Kinetic.Text({
		    	x: clock.getX(),
		    	y: clock.getY() + 35,
		    	text: '60',
		    	fontSize: 56,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });
		    
		    var marco = new Kinetic.Image({
			   x: screenWidth / 2 - 199,
			   y: 80,
			   image: images.marco,
			   height: 100
		    });
		    
		    var marcoText = new Kinetic.Text({
			    x: screenWidth / 2 - 199,
			    y: 100,
			    fontSize: 56,
			    fontFamily: 'Chicken Butt',
			    fill: '#555',
			    align: 'center',
			    width: 398
		    });
		    
		    var streakText = new Kinetic.Text({
		    	x:	clock.getX(),
		    	y:	clock.getY() + 85,
		    	text: 'x0',
		    	fontSize: 20,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });

		    var scoreText = new Kinetic.Text({
		    	x: 50,
		    	y: 20,
		    	text: "0",
		    	fontSize: 56,
		    	fontFamily: "Chicken Butt",
		    	fill: "#555"
		    });
		    
		    var yeti = new Kinetic.Sprite({
			   image: images.yeti_stage5,
			   x: 22 * screenWidthFactor,
			   y: screenHeight - 120 * screenHeightFactor,
			   animations: ANIMATIONS.STAGE5.YETI,
			   animation: 'idle',
			   frameRate: 21,
/* 			   index: 0, */
			   scale: [0.3 * screenWidthFactor, 0.3 * screenHeightFactor]
			});
		    
		    
		    /**
		     *
		     *  Add images to layers
		     *  and layers to the stage
		     **/
		    
		    backgroundLayer.add(bg);
		    
		    shapesLayer.add(clock);
		    shapesLayer.add(timerText);
		    
		    shapesLayer.add(marco);
		    shapesLayer.add(marcoText);
		    
		    shapesLayer.add(streakText);
		    shapesLayer.add(scoreText);
		    
		    shapesLayer.add(yeti);
			
			yeti.start();

		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
		    
		    var countryImages = {};
		    
		    
		    // Generate random countries
		    var randoms = [];
		    var levels = [];
		    var getRandom = function () {
			    return Math.random() * 1000 % Object.size(sourceFlags);
		    }
		    
			while (randoms.length < 180) {
				var randomnumber = Math.floor(getRandom());
				var found = false;
				for (var i=0; i < randoms.length; i++) {
					if (randoms[i] == randomnumber) {
						found = true;
						break;
					}
				}
				if(!found) {
					randoms[randoms.length] = randomnumber;
				}
			}
			
			var ok = 0;
			
			for (var i = 0; i < randoms.length; i++) {
				
				var mod = i % 6;
				var div = Math.floor(i / 6);
				
				var key = Object.keys(sourceFlags)[randoms[i]];
				var val = sourceFlags[key].split('.')[0];
				
				if (mod == 0) {
					
					ok = Math.floor(Math.random() * 1000 % 6);
					
					levels.push({
						flags: [{
							key: key,
							val: val
						}],
						correct: {}
					});
				} else {
					levels[div].flags.push({
						key: key,
						val: val
					});
				}
				
				if (mod == ok) {
					levels[div].correct = val;
				}
				
			}
		    
		    
		    var answers = [
		    	{
			    	flags: [
			    		'argentina',
			    		'cuba',
			    		'chile',
			    		'canada',
			    		'denmark',
			    		'cyprus'
			    	],
			    	correct: 'argentina'
		    	},
		    	{
			    	flags: [
			    		'australia',
			    		'chad',
			    		'burundi',
			    		'eritrea',
			    		'egipto',
			    		'el salvador'
			    	],
			    	correct: 'egipto'
		    	},
		    	{
			    	flags: [
			    		'cook islands',
			    		'comoros',
			    		'benin',
			    		'bermuda',
			    		'cambodia',
			    		'central african republic'
			    	],
			    	correct: 'comoros'
		    	},
		    	{
			    	flags: [
			    		'djibouti',
			    		'east timor',
			    		'cape verde',
			    		'bosnia and herzegovina',
			    		'bhutan',
			    		'brazil'
			    	],
			    	correct: 'djibouti'
		    	},
		    	{
			    	flags: [
			    		'fiji',
			    		'estonia',
			    		'ecuador',
			    		'bulgaria',
			    		'cameroon',
			    		'bahrain'
			    	],
			    	correct: 'bulgaria'
		    	},
		    	{
			    	flags: [
			    		'botswana',
			    		'china',
			    		'bolivia',
			    		'cocos islands',
			    		'colombia',
			    		'barbados'
			    	],
			    	correct: 'botswana'
		    	},
		    	{
			    	flags: [
			    		'brunei',
			    		'bahamas, the',
			    		'armenia',
			    		'aruba',
			    		'costa rica',
			    		'andorra'
			    	],
			    	correct: 'costa rica'
		    	}
		    ];
		    
		    answers = level1;
		    
		    var currentFlagLevel = 0;
		    var correctStreak = 0;
		    score = 0;
		    var correctFlags = [];
		    
		    marcoText.setText(levels[currentFlagLevel].correct);
		    
		    function isCorrectAnswer(name) {
			    if (name == levels[currentFlagLevel].correct)
			    	return true;
			    return false;
		    }
		    
		    for (var i in flags) {
			    
			    countryImages[i] = new Kinetic.Image({
			    	image: flags[i],
			    	width: 140 * screenWidthFactor,
			    	height: 80 * screenHeightFactor
			    });
			    countryImages[i]['countryName'] = sourceFlags[i].split('.')[0];
			    countryImages[i]['countryFile'] = i;
			    
			    countryImages[i].on('mousedown touchstart', function () {
				    sounds.swoosh.play();
				    if (isCorrectAnswer(this.countryName)) {
						
						yeti.setAnimation('hooray');
						
						yeti.afterFrame(19, function () {
							yeti.setAnimation('idle');
						});
						
					    correctStreak++;
					    // Update streak
					    streakText.setText('x'+correctStreak);
					    // Update score
					    score += (10 - parseInt(timerText.getText()) % 10 ) * 10 * Math.round(Math.random() * 10);
					    
					    // Max = 30 * 1000 = 30000
					    
					    scoreText.setText(score);

						tellScore(score);
					    // Add country to flag stack
					    correctFlags.push(this);
					    
					    // Start a new correct streak
					    function wipeCorrectStreak() {
					    
					    
					    	$(correctFlags).each( function (i, flag) {
						    	console.log(flag);
						    	var tween = new Kinetic.Tween({
						    		node: 		flag,
							    	opacity:	0,
									duration:	1,
									onFinish:	function () {
										this.destroy();
										
										if (flag === correctFlags[length-1]) {
											correctFlags = [];
										}
										
/* 										correctFlags = []; */
									}
								});
								
								tween.play();
					    	});
					    }
					    
					    // Send it to the correct streak column
					    var tween = new Kinetic.Tween({
					    
							node:		this,
							x:			screenWidth - 140 * screenWidthFactor,
							y:			clock.getY() + 150 + ((correctStreak - 1) % 5) * 55 * screenHeightFactor,
							width:		84 * screenWidthFactor,
							height:		48 * screenHeightFactor,
							opacity:	1,
							duration:	1,
							onFinish:	function () {
								this.destroy();
								if ( correctStreak % 5 == 0 ) {
								    console.log('x'+correctStreak);
								    
								    // Update timer +5 sec
								    timerText.setText( parseInt(timerText.getText()) + 5 );
								    // Up the score
								    score += 120;
								    scoreText.setText(score);
								    
								    wipeCorrectStreak();
							    }
							}
						});
						
						tween.play();
						
						removeFlagLevel(true);
				    
				    } else {
					    streakText.setText('x0');
					    wipeCorrectStreak();
					    correctStreak = 0;
					    removeFlagLevel(false);
				    }
				    
			    });

		    }
		    
		    function removeFlagLevel(isCorrect) {
		    
			    levels[currentFlagLevel].flags.forEach( function (f, i) {
			    	
			    	
			    	var tween = new Kinetic.Tween({
						node:		countryImages[f.key],
						opacity: 	0,
						duration:	.3,
						onFinish:	function () {
							
/* 							countryImages[f.key].remove(); */
							if (i == 5) {
								currentFlagLevel++;
								displayFlagLevel();
								// Update to next country
								marcoText.setText(levels[currentFlagLevel].correct);
								
							}
						}
					});
					
					if (!isCorrect ) {
						tween.play();
					} else {
						if (!isCorrectAnswer(f.val)) {
							tween.play();
						}
					}
			    
			    });
		    }
		    
		    function displayFlagLevel() {
			    levels[currentFlagLevel].flags.forEach( function (f, i) {
				    
				    var y = i > 2 ? 400 : 250;
				    var pos = i % 3;
				    var factor = 180 * screenWidthFactor;
				    var offset = 120 * screenWidthFactor;
				    countryImages[f.key].setX( pos * factor + offset );
				    countryImages[f.key].setY( y * screenHeightFactor );
				    
				    shapesLayer.add(countryImages[f.key]);
			    });
		    }
		    
		    displayFlagLevel();

		    /**
		     *
		     *  Start the timer for the countdown
		     */
		     
		    function timerDidStop() {
/* 			    transitionFadeOut(LEVEL.LEVEL2); */
/* 				Game.gotoLevelLoader(LEVEL.LEVEL2); */
				target = LEVEL.LEVEL2;
				transitionFadeOut(LEVEL.LEVELLOADER);
		    }

		    function tick() {
		    	timerText.setText(parseInt(timerText.getText()) - 1);
		    	sounds.tick.play();
		    	if (timerText.getText() == "0") {
		    		clearInterval(interval);
		    		timerDidStop();
		    	}
		    }

		    var interval = setInterval(tick, timers[0]);
		}, 
		
		/*
		 *
		 * END of Level 1
		 *
		 * Beginning of Level 2
		 *
		 * El del contorno de los paises
		 * 
		 */
		
		gotoLevel2: function () {
			
			console.log("Level 2");
			
			shapesLayer.add(winPlaceholder);
			shapesLayer.add(winText);
			
			var bg = new Kinetic.Image({
			   image: images.etapa2,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight
/* 			   filter: Kinetic.Filters.Colorize */
		    });
		    
/* 		    bg.setFilterColorizeColor([10,20,30]); */
		    
		    var clock = new Kinetic.Image({
		    	image: images.clock,
		    	x: screenWidth - 160,
		    	y: 20
		    });
		    
		    var marco = new Kinetic.Image({
			   x: screenWidth / 10,
			   y: clock.getY() + clock.getHeight(),
			   image: images.papiro,
			   height: screenHeight * 0.7,
			   width: screenWidth * 0.6
		    });
		    
		    var timerText = new Kinetic.Text({
		    	x: clock.getX(),
		    	y: clock.getY() + 35,
		    	text: '60',
		    	fontSize: 56,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });

		    var option1 = new Kinetic.Image({
		    	x: marco.getX() + marco.getWidth() + 20 * screenWidthFactor,
		    	y: screenHeight / 2 - screenHeight * 0.2,
		    	image: images.papiro_con,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option2 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option1.getY() + option1.getHeight() + 10 * screenHeightFactor,
		    	image: images.papiro_con,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option3 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option2.getY() + option2.getHeight() + 10 * screenHeightFactor,
		    	image: images.papiro_con,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option4 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option3.getY() + option3.getHeight() + 10 * screenHeightFactor,
		    	image: images.papiro_con,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option1Text = new Kinetic.Text({
		    	x: option1.getX(),
		    	y: option1.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option1.getWidth()
		    });

		    var option2Text = new Kinetic.Text({
		    	x: option2.getX(),
		    	y: option2.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option2.getWidth()
		    });

		    var option3Text = new Kinetic.Text({
		    	x: option3.getX(),
		    	y: option3.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option3.getWidth()
		    });

		    var option4Text = new Kinetic.Text({
		    	x: option4.getX(),
		    	y: option4.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option4.getWidth()
		    });

		    var funny = new Kinetic.Image({
		    	x: marco.getX() + 73 * screenWidthFactor,
		    	y: marco.getY() + 78 * screenHeightFactor,
		    	image: images.mapbox,
		    	width: marco.getWidth() - 137 * screenWidthFactor,
		    	height: marco.getHeight() - 137 * screenHeightFactor,
		    	opacity: 0,
		    	rotationDeg: -1
		    });
		    
		    var water = new Kinetic.Rect({
			    x: marco.getX() + 73 * screenWidthFactor,
		    	y: marco.getY() + 78 * screenHeightFactor,
		    	width: marco.getWidth() - 137 * screenWidthFactor,
		    	height: marco.getHeight() - 137 * screenHeightFactor,
/* 		    	opacity: 0, */
		    	rotationDeg: -1,
		    	fill: "#2980b9"
		    });
		    
		    var scoreText = new Kinetic.Text({
		    	x: 50,
		    	y: 20,
		    	text: score,
		    	fontSize: 56,
		    	fontFamily: "Chicken Butt",
		    	fill: "#555"
		    });
		    
		    var streakText = new Kinetic.Text({
		    	x:	clock.getX(),
		    	y:	clock.getY() + 85,
		    	text: 'x0',
		    	fontSize: 20,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });
		    
		    var yeti = new Kinetic.Sprite({
			   image: images.yeti_stage2,
			   x: 22 * screenWidthFactor,
			   y: screenHeight - 120 * screenHeightFactor,
			   animations: ANIMATIONS.STAGE2.YETI,
			   animation: 'idle',
			   frameRate: 21,
/* 			   index: 0, */
			   scale: [screenWidthFactor / 2, screenHeightFactor / 2]
			});
		    
		    
		    var allCoords = Country.getAllCoords();
		    
		    // Generate random countries
		    var randoms = [];
		    var levels = [];
		    var getRandom = function () {
			    return Math.random() * 1000 % allCoords.length;
		    }
		    
			while (randoms.length < 100) {
				var randomnumber = Math.floor(getRandom());
				var found = false;
				for (var i=0; i < randoms.length; i++) {
					if (randoms[i] == randomnumber) {
						found = true;
						break;
					}
				}
				if(!found) {
					randoms[randoms.length] = randomnumber;
/* 					console.log(Country.getCountryByIndex(randomnumber).properties.name); */
				}
			}
		    
		    // Fill up the levels
		    for (var i = 0; i < randoms.length; i++) {
			    var correctIndex = Math.floor(Math.random() * 100 % 4);
			    
			    levels.push({
				    choices: [],
				    correctIndex: correctIndex
			    });
			    
			    var otherChoices = [];
			    
			    while (otherChoices.length < 3) {
					var randomnumber = Math.floor(getRandom());
					var found = false;
					for (var j = 0; j < otherChoices.length; j++) {
						if (otherChoices[j] == randomnumber) {
							found = true;
							break
						}
					}
					if(!found) {
						otherChoices[otherChoices.length] = randomnumber;
					}
				}
			    var used = false;
			    for (var j = 0 ; j < 4; j++) {
				    
				    if (j == correctIndex) {
					    levels[i].choices.push(Country.getCountryByIndex(randoms[i]).properties.name);
					    used = true;
				    } else {
					    levels[i].choices.push(Country.getCountryByIndex(otherChoices[ ( (used == true) ? j-1 : j ) ]).properties.name);
					    
					}
			    }
		    }
		    
/* 		    polysLayer.setOpacity(0); */
		    
		    for (var i = 0; i < allCoords.length; i++) {
			    
			    allCoords[i] = Adapter.convertCoords(allCoords[i], funny.getWidth(), funny.getHeight());
			    
			    for (var j = 0; j < allCoords[i].length; j++) {
				    
				    var polygon = new Kinetic.Polygon({
				    	points: allCoords[i][j][0],
				    	fill: '#ecf0f1',
				    	stroke: 'black',
				    	strokeWidth: 0.5,
				    	x: funny.getX() + funny.getWidth() / 2,
				    	y: funny.getY() + funny.getHeight() / 2,
				    	name: i
				    });
				    
				    polysLayer.add(polygon);
				}
			    
		    }
		    
		    
		    answers = level2;

		    

		    /*
		     * Add onto screen
		     *
		     */
		    
		    shapesLayer.add(scoreText);
		     
		    shapesLayer.add(option1);
		    shapesLayer.add(option2);
		    shapesLayer.add(option3);
		    shapesLayer.add(option4);

		    shapesLayer.add(option1Text);
		    shapesLayer.add(option2Text);
		    shapesLayer.add(option3Text);
		    shapesLayer.add(option4Text);

		    shapesLayer.add(marco);
		    shapesLayer.add(funny);
		    shapesLayer.add(water);

		    shapesLayer.add(clock);
		    shapesLayer.add(timerText);
		    shapesLayer.add(streakText);
		    
		    shapesLayer.add(yeti);
		    
		    yeti.start();
		    
		    backgroundLayer.add(bg);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    stage.add(polysLayer);
			
			var currentCountryLevel = 0;
			var currentStreak = 0;

		    function validateAnswer(answer) {
		    	return (answer == levels[currentCountryLevel-1].correctIndex);
		    }
		    
		    /*
		     * Tweening
		     *
		     */
		     
		    var resetAnimation = function (a) {
			    a.stop();
			    a.frame.time = 0;
			    
/* 			    polysLayer.getChildren().each( function (poly, n) { */
/* 			    	poly.setOpacity(0); */
/* 		    	}); */
/* 				polysLayer.setOpacity(0); */
			    
			    polysLayer.get('.' + randoms[currentCountryLevel]).each(function (p,n) {
				    p.setFill('#e74c3c');
				    p.setOpacity(1);
			    });
			    
			    
			    if (currentCountryLevel != 0) {
				    polysLayer.get('.' + randoms[currentCountryLevel-1]).each(function (p,n) {
					    p.setFill('#ecf0f1');
/* 					    p.setOpacity(0); */
				    });
			    }
			    
			    // Update labels
			    option1Text.setText(levels[currentCountryLevel].choices[0]);
			    option2Text.setText(levels[currentCountryLevel].choices[1]);
			    option3Text.setText(levels[currentCountryLevel].choices[2]);
			    option4Text.setText(levels[currentCountryLevel].choices[3]);
			    
			    // Update Score
			    
			    if (score != scoreText.getText()) {
				    scoreText.setText(score);
				    tellScore(score);
				}
			    
		    	// Goto next step
			    currentCountryLevel++;
			    
			    setTimeout(function (){a.start()},300);
			    
			    
		    };
		    
		    
		    var animTime = 10;
		    
		    var anim = new Kinetic.Animation( function(frame) {
		    	
		    	if ( frame.time < animTime * 1000) {
			    	
/* 					polysLayer.setOpacity(polysLayer.getOpacity() + frame.timeDiff / (animTime * 1000)); */
			    	
		    	}
		    	else {
		    		currentStreak = 0;
			    	resetAnimation(anim);
			    }
		    }, polysLayer);
		    
			resetAnimation(anim);
		    
		    function displayNextCountries() {
			    
			    // Update streak
			    streakText.setText('x'+currentStreak);
			    resetAnimation(anim);
		    }
		    
		    
		    

		    /*
		     * Events
		     *
		     */
		     
		    function optionHandler(index) {
			    console.log(validateAnswer(index) ? 'Correcto' : 'Falso');
			    sounds.swoosh.play();
		    	if (validateAnswer(index)) {
		    	
		    		yeti.setAnimation('hooray');
		    		
		    		yeti.afterFrame(19, function () {
			    		yeti.setAnimation('idle');
		    		});
		    	
			    	score += Math.round((animTime * 1000 - anim.frame.time) / 10); // 1000 * 30 = 30000
			    	currentStreak++;
		    	} else {
			    	currentStreak = 0;
		    	}
		    	displayNextCountries();
		    }
		     
		    option1.on("mousedown touchstart", function () {
		    	optionHandler(0);
		    });
		    
		    option1Text.on("mousedown touchstart", function () {
		    	optionHandler(0);
		    });

		    option2.on("mousedown touchstart", function () {
		    	optionHandler(1);
		    });
		    
		    option2Text.on("mousedown touchstart", function () {
		    	optionHandler(1);
		    });

		    option3.on("mousedown touchstart", function () {
		    	optionHandler(2);
		    });
		    
		    option3Text.on("mousedown touchstart", function () {
		    	optionHandler(2);
		    });

		    option4.on("mousedown touchstart", function () {
		    	optionHandler(3);
		    });
		    
		    option4Text.on("mousedown touchstart", function () {
		    	optionHandler(3);
		    });


		    /*
		     * Clock handling
		     *
		     */

		    function timerDidStop() {
		    	anim.stop();
		    	anim = {};
		    	target = LEVEL.LEVEL3;
			    transitionFadeOut(LEVEL.LEVELLOADER);
		    }

		    function tick() {
		    	timerText.setText(parseInt(timerText.getText()) - 1);
		    	sounds.tick.play();
		    	if (timerText.getText() == "0") {
		    		clearInterval(interval);
		    		timerDidStop();
		    	}
		    }

		    var interval = setInterval(tick, timers[1]);
		},
		
		/*
		 *
		 * END of Level 2
		 *
		 * Beginning of Level 3
		 *
		 * Distance to place
		 *
		 */
		
		gotoLevel3: function () {

			console.log("Level 3");
			
			shapesLayer.add(winPlaceholder);
			shapesLayer.add(winText);
			
			var bg = new Kinetic.Image({
			   image: images.etapa3,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight,
/* 			   filter: Kinetic.Filters.Colorize */
		    });
		    
/* 		    bg.setFilterColorizeColor([30,20,10]); */
		    
		    var clock = new Kinetic.Image({
		    	image: images.clock,
		    	x: screenWidth - 160,
		    	y: 20
		    });
		    
		    var next = new Kinetic.Image({
			   x: screenWidth / 2 - 199,
			   y: 70,
			   image: images.marco,
			   height: 100
		    });
		    
		    var nextText = new Kinetic.Text({
			    x: screenWidth / 2 - 199,
			    y: 90,
			    fontSize: 56,
			    fontFamily: 'Chicken Butt',
			    fill: '#555',
			    align: 'center',
			    width: 398
		    });
		    
		    var marco = new Kinetic.Image({
			   x: screenWidth / 10,
			   y: clock.getY() + clock.getHeight() + 10 * screenHeightFactor,
			   image: images.marco,
			   height: screenHeight * 0.7,
			   width: screenWidth * 0.8
		    });
		    
		    var timerText = new Kinetic.Text({
		    	x: clock.getX(),
		    	y: clock.getY() + 35,
		    	text: '60',
		    	fontSize: 56,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });

		    var option1 = new Kinetic.Image({
		    	x: marco.getX() + marco.getWidth() + 20 * screenWidthFactor,
		    	y: screenHeight / 2 - screenHeight * 0.2,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option2 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option1.getY() + option1.getHeight() + 10 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option3 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option2.getY() + option2.getHeight() + 10 * screenHeightFactor,
		    	image: images.button,
		    	width: 179 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option4 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option3.getY() + option3.getHeight() + 10 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option1Text = new Kinetic.Text({
		    	x: option1.getX(),
		    	y: option1.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option1.getWidth()
		    });

		    var option2Text = new Kinetic.Text({
		    	x: option2.getX(),
		    	y: option2.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option2.getWidth()
		    });

		    var option3Text = new Kinetic.Text({
		    	x: option3.getX(),
		    	y: option3.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option3.getWidth()
		    });

		    var option4Text = new Kinetic.Text({
		    	x: option4.getX(),
		    	y: option4.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option4.getWidth()
		    });

		    
/* 		    funny.setOpacity(0); */
		    
		    var water = new Kinetic.Rect({
			    x: marco.getX() + 5 * screenWidthFactor,
		    	y: marco.getY() + 5 * screenHeightFactor,
		    	width: marco.getWidth() - 10 * screenWidthFactor,
		    	height: marco.getHeight() - 10 * screenHeightFactor,
/* 		    	opacity: 0, */
/* 		    	rotationDeg: -1, */
		    	fill: "#2980b9"
		    });
		    
		    var scoreText = new Kinetic.Text({
		    	x: 50,
		    	y: 20,
		    	text: score,
		    	fontSize: 56,
		    	fontFamily: "Chicken Butt",
		    	fill: "#555"
		    });
		    
		    var streakText = new Kinetic.Text({
		    	x:	clock.getX(),
		    	y:	clock.getY() + 85,
		    	text: 'x0',
		    	fontSize: 20,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });
		    
		    var circle = new Kinetic.Circle({
			    	
		    	radius: 5 * screenHeightFactor,
		    	fill: 'red',
		    	stroke: 'black',
		    	strokeWidth: 0.01,
	    		opacity: 0,
	    		x: 0,
	    		y: 0
	    	});
	    	
	    	var answerCircle = new Kinetic.Circle({
		    	
		    	radius: 5 * screenHeightFactor,
		    	fill: 'blue',
		    	stroke: 'black',
		    	strokeWidth: 0.01,
	    		opacity: 0,
	    		x: 0,
	    		y: 0
	    	});

	    	var greenLine = new Kinetic.Line({
	    		stroke: 'green',
	    		strokeWidth: 2,
	    		lineJoin: 'round',
	    		/*
	    		 * line segments with a length of 33px
	    		 * with a gap of 10px
	    		 */
	    		dashArray: [33, 10],
	    		opacity: 0,
	    		points: [0,1,2,3]
	    	});
			
			polysLayer.setOpacity(1);
			
			function touchHandler() {
				sounds.swoosh.play();
		    	
		    	var mouseXY = stage.getMousePosition();
		    	var position = new Point(mouseXY.x - water.getX(), mouseXY.y - water.getY());
		   
		    	//console.log(euclideanDistance(position, pt2));
		    	
		    	console.log(cities[currentAnswer]['coords']);
		    	
		    	// Draw line
		    	greenLine.setPoints([
		    		position.x + water.getX(),
		    		position.y + water.getY(),
		    		cities[currentAnswer]['coords'][0] + water.getX() + water.getWidth() / 2,
		    		cities[currentAnswer]['coords'][1] + water.getY() + water.getHeight() / 2]);
		    		
		    		
		    	// Draw hover circle
		    	
		    	circle.setX(position.x + water.getX());
		    	circle.setY(position.y + water.getY());
		    	
		    	answerCircle.setX(cities[currentAnswer]['coords'][0] + water.getX() + water.getWidth() / 2);
		    	//answers[currentAnswer].x + funny.getX());
		    	answerCircle.setY(cities[currentAnswer]['coords'][1] + water.getY() + water.getHeight() / 2);
		    	//answers[currentAnswer].y + funny.getY());
		    	
/* 		    	polysLayer.add(circle); */
/* 		    	polysLayer.add(answerCircle); */
/* 		    	polysLayer.add(greenLine); */
		    	circle.moveToTop();
		    	answerCircle.moveToTop();
		    	greenLine.moveToTop();
		    	circle.setOpacity(1);
		    	answerCircle.setOpacity(1);
		    	greenLine.setOpacity(1);
		    	
		    	polysLayer.draw();
		    	
		    	currentAnswer++;
		    	nextText.setText(cities[currentAnswer]['name']);
		    	
		    	var adding = Math.round(
		    			Math.round(
		    				Math.sqrt(screenWidth * screenWidth + screenHeight * screenHeight) -
		    				euclideanDistance(position, new Point(
		    					cities[currentAnswer]['coords'][0],
		    					cities[currentAnswer]['coords'][1]))
		    			) * Math.random() * 1000 % 1000);

		    	score += adding;

		    	console.log(adding);


		    	if (adding < 1000)
		    		console.log('Muy mal');
		    	else if (adding < 1100)
		    		console.log('mal');
		    	else if (adding < 1200)
		    		console.log('bien')
		    	else if (adding < 1300)
		    		console.log('bastante bien');
		    	else if (adding < 1300)
		    		console.log('muy bien');
		    	else if (adding < 1500)
		    		console.log('excelente');
		    	else if (adding < 1600)
		    		console.log('impresionante');
		    	
		    	scoreText.setText(score);
		    	tellScore(score);
		    	
		    	function quickWipe() {
			    	circle.setOpacity(0);
			    	answerCircle.setOpacity(0);
			    	greenLine.setOpacity(0);
			    	polysLayer.draw();
		    	}
		    	
		    	setTimeout(quickWipe, 500);
			}
			
		    var allCoords = Country.getAllCoords();
			
/* 			var polysLayer = new Kinetic.Layer(); */
/* 		    polysLayer.setOpacity(0); */
		    
		    for (var i = 0; i < allCoords.length; i++) {
			    
			    allCoords[i] = Adapter.convertCoords(allCoords[i], water.getWidth(), water.getHeight());
			    
			    for (var j = 0; j < allCoords[i].length; j++) {
			    
				    var polygon = new Kinetic.Polygon({
				    	points: allCoords[i][j][0],
				    	fill: '#ecf0f1',
				    	stroke: 'black',
				    	strokeWidth: 0.5,
				    	x: water.getX() + water.getWidth() / 2,
				    	y: water.getY() + water.getHeight() / 2,
				    	name: i
				    });
				    
				    polygon.on('mousedown touchstart', touchHandler);				    
				    polysLayer.add(polygon);
				}
			    
		    }
		    
		    
		    /*
		     *
		     * Make levels
		     *
		     */
		    
		    var allCities = Country.getCities();
		    
		    for (var i = 0; i < allCities.length; i++) {
			    
			    allCities[i].geometry.coordinates = Adapter.convertPoints(allCities[i].geometry.coordinates, water.getWidth(), water.getHeight());
		    }
		    
		    var cities = [];
		    
		    var getRandom = function () {
			    return Math.random() * 1000 % allCities.length;
		    }
		    
		    while (cities.length < 100) {
				var randomnumber = Math.floor(getRandom());
				var found = false;
				for (var i=0; i < cities.length; i++) {
					if (cities[i] == randomnumber) {
						found = true;
						break;
					}
				}
				if(!found) {
					var level = {
						name: decodeURIComponent(allCities[randomnumber].properties.city),
						coords: allCities[randomnumber].geometry.coordinates
					};
					cities[cities.length] = level;
				}
			}
			

		    /*
		     * Add onto screen
		     *
		     */

		    //shapesLayer.add(greenLine);
		    
		    shapesLayer.add(next);
		    shapesLayer.add(nextText);
		    shapesLayer.add(scoreText);

		    shapesLayer.add(marco);
/* 		    shapesLayer.add(funny); */
/* 		    shapesLayer.add(dinosaur); */
/* 		    shapesLayer.add(dinosaurShape); */

		    shapesLayer.add(clock);
		    shapesLayer.add(timerText);
		    shapesLayer.add(streakText);
		    shapesLayer.add(water);

		    backgroundLayer.add(bg);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
		    polysLayer.add(circle);
		    polysLayer.add(answerCircle);
		    polysLayer.add(greenLine);
		    stage.add(polysLayer);
		    
		    
		    /*

		    var answers = [
		    	{
			    	name: "Argentina",
			    	x: 100,
			    	y: 100
		    	},
		    	{
			    	name: "Chile",
			    	x: 200,
			    	y: 120
		    	},
		    	{
			    	name: "Belgica",
			    	x: 300,
			    	y: 150
		    	},
		    	{
			    	name: "Brasil",
			    	x: 500,
			    	y: 250
		    	},
		    	{
			    	name: "Turquia",
			    	x: 600,
			    	y: 450
		    	},
		    	{
			    	name: "Zimbawe",
			    	x: 400,
			    	y: 150
		    	},
		    	{
			    	name: "Holanda",
			    	x: 250,
			    	y: 150
		    	},
		    	{
			    	name: "Polonia",
			    	x: 300,
			    	y: 125
		    	},
		    ];
		    
*/
		    answers = level3;
		    
		    var currentAnswer = 0;
		    
		    nextText.setText(cities[currentAnswer]['name']);//answers[currentAnswer].name);
		    scoreText.setText(score);
			
			var currentCountryLevel = 0;
			var currentStreak = 0;

		    function displayNextCountries() {
			    
			    // Update score
		    	scoreText.setText(score);
		    	
		    	// Goto next step
			    currentCountryLevel++;
			    
			    
			    // Update labels
			    
			    // Update streak
			    streakText.setText('x'+currentStreak);
			    	
		    }
		    
		    /*
		     * Tweening
		     *
		     */

		    /*
		     * Events
		     *
		     */
		     
		    function Point(x, y) {
			    
			    this.x = x;
			    this.y = y;
		    }
		    
		    function euclideanDistance(pt1, pt2) {
			    
			    return Math.sqrt( Math.pow(pt2.x - pt1.x, 2) + Math.pow(pt2.y - pt1.y, 2) );
		    }
		    
		    water.on("mousedown touchstart", touchHandler);
		    


		    /*
		     * Clock handling
		     *
		     */

		    function timerDidStop() {
		    
		    	transitionFadeOut(LEVEL.LEVEL4);
		    }

		    function tick() {
		    	timerText.setText(parseInt(timerText.getText()) - 1);
		    	sounds.tick.play();
		    	if (timerText.getText() == "0") {
		    		clearInterval(interval);
		    		timerDidStop();
		    	}
		    }

		    var interval = setInterval(tick, timers[2]);
			
		},
		
		/*
		 *
		 * END of Level 3
		 *
		 * Beginning of Level 4
		 *
		 * Monumentos
		 *
		 */
		
		gotoLevel4: function () {
			
			console.log("Level 4");
			
			shapesLayer.add(winPlaceholder);
			shapesLayer.add(winText);
			
			var bg = new Kinetic.Image({
			   image: images.stage4_back,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight
/* 			   filter: Kinetic.Filters.Colorize */
		    });
		    
/* 		    bg.setFilterColorizeColor([30,10,20]); */
		    		    
		    var clock = new Kinetic.Image({
		    	image: images.clock,
		    	x: screenWidth - 160,
		    	y: 20
		    });
		    
		    var marco = new Kinetic.Image({
/* 			   x: screenWidth / 2 - marco.getWidth() / 2, */
			   y: clock.getY() + clock.getHeight() - 45 * screenHeightFactor,
			   image: images.marco,
			   height: screenHeight * 0.65,
			   width: screenWidth * 0.7
		    });
		    
		    marco.setX(screenWidth / 2 - marco.getWidth() / 2);		    
		    
		    var timerText = new Kinetic.Text({
		    	x: clock.getX(),
		    	y: clock.getY() + 35,
		    	text: '60',
		    	fontSize: 56,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });

		    var option1 = new Kinetic.Image({
		    	x: screenWidth / 2 - 200 * screenWidthFactor,
		    	y: screenHeight * 0.76,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option2 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option1.getY() + option1.getHeight() + 7 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option3 = new Kinetic.Image({
		    	x: option1.getX() + 200 * screenWidthFactor,
		    	y: option1.getY(),
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option4 = new Kinetic.Image({
		    	x: option3.getX(),
		    	y: option3.getY() + option3.getHeight() + 7 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option1Text = new Kinetic.Text({
		    	x: option1.getX() + 2 * screenWidthFactor,
		    	y: option1.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 28,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option1.getWidth() - 10 * screenWidthFactor
		    });

		    var option2Text = new Kinetic.Text({
		    	x: option2.getX() + 2 * screenWidthFactor,
		    	y: option2.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 28,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option2.getWidth() - 10 * screenWidthFactor
		    });

		    var option3Text = new Kinetic.Text({
		    	x: option3.getX() + 2 * screenWidthFactor,
		    	y: option3.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 28,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option3.getWidth() - 10 * screenWidthFactor
		    });

		    var option4Text = new Kinetic.Text({
		    	x: option4.getX() + 2 * screenWidthFactor,
		    	y: option4.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 28,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option4.getWidth() - 10 * screenWidthFactor
		    });

		    var funny = new Kinetic.Image({
		    	x: marco.getX() + 5,
		    	y: marco.getY() + 5,
		    	image: images.funny,
		    	width: marco.getWidth() - 10,
		    	height: marco.getHeight() - 10,
		    	opacity: 0
		    });
		    
		    funny.setOpacity(0);
		    
		    var scoreText = new Kinetic.Text({
		    	x: 50,
		    	y: 25,
		    	text: score,
		    	fontSize: 56,
		    	fontFamily: "Chicken Butt",
		    	fill: "#555"
		    });
		    
		    var streakText = new Kinetic.Text({
		    	x:	clock.getX(),
		    	y:	clock.getY() + 85,
		    	text: 'x0',
		    	fontSize: 20,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });
			
			var totem = new Kinetic.Image({
				x: 15 * screenWidthFactor,
				y: screenHeight - 175 * screenHeightFactor,
				image: images.totem,
				width: 66 * screenWidthFactor,
				height: 173 * screenHeightFactor
			});
			
			var totem2 = new Kinetic.Image({
				x: screenWidth - 80 * screenWidthFactor,
				y: totem.getY(),
				image: images.totem,
				width: 66 * screenWidthFactor,
				height: 173 * screenHeightFactor
			});
			
			var casa = new Kinetic.Image({
				x: -100 * screenWidthFactor,
				y: -20 * screenHeightFactor,
				image: images.casa,
				width: 329 * screenWidthFactor,
				height: 329 * screenHeightFactor
			});
			
			var yeti = new Kinetic.Sprite({
			   image: images.yeti_stage4,
			   x: 22 * screenWidthFactor,
			   y: screenHeight - 120 * screenHeightFactor,
			   animations: ANIMATIONS.STAGE4.YETI,
			   animation: 'idle',
			   frameRate: 21,
/* 			   index: 0, */
			   scale: [screenWidthFactor / 2, screenHeightFactor / 2]
			});
			
			
			// Generate random countries
		    var randoms = [];
		    var levels = [];
		    var getRandom = function () {
			    return Math.random() * 1000 % Object.size(sourceMonuments);
		    }
		    
			while (randoms.length < Object.size(sourceMonuments)) {
				var randomnumber = Math.floor(getRandom());
				var found = false;
				for (var i=0; i < randoms.length; i++) {
					if (randoms[i] == randomnumber) {
						found = true;
						break;
					}
				}
				if(!found) {
					randoms[randoms.length] = randomnumber;
				}
			}
			
			var ok = 0;
			
			
			for (var i = 0; i < randoms.length; i++) {
				
				var mod = i % 4;
				var div = Math.floor(i / 4);
				
				var key = Object.keys(sourceMonuments)[randoms[i]];
				var val = sourceMonuments[key].split('.')[0];
				
				if (mod == 0) {
					
					ok = Math.floor(Math.random() * 1000 % 4);
					
					var v = (ok == 0) ? [val] : [];
					
					levels.push({
						options: [val],
						correctIndex: ok
					});
				} else {
					levels[div].options.push(val);
				}
				
				if (mod == ok) {
					levels[div].i = monuments[key];
				}
				
			}
			
			console.log(levels);


		    var answers = [
		    	{
		    		options: [
		    			"Momumento Rushmore",
		    			"Piramide de Guiza",
		    			"Amazonas",
		    			"Templos Mayas"
		    		],
		    		correct: "argentina",
		    		i: images.guiza,
		    		correctIndex: 1
		    	},
		    	{
		    		options: [
		    			"Estatua de la Libertad",
		    			"Machu Pichu",
		    			"Isla de Pascua",
		    			"Templo del Primer Emperador Chino"
		    		],
		    		correct: "francia",
		    		i: images.estatua,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Torre Eifel",
		    			"Arco de la victoria",
		    			"Louvre",
		    			"Arco del triunfo"
		    		],
		    		correct: "rusia",
		    		i: images.eifel,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Torre de Pisa",
		    			"Cataratas del Iguazu",
		    			"Cataratas del Niagara",
		    			"Capilla Sixtina"
		    		],
		    		correct: "oman",
		    		i: images.pisa,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Opera de Sidney",
		    			"Petra",
		    			"Palacio de Buckinham",
		    			"Big Ben"
		    		],
		    		correct: "tunes",
		    		i: images.opera,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Muralla China",
		    			"Kremlin",
		    			"Chichen Itza",
		    			"Estatua de Zeus"
		    		],
		    		correct: "tunes",
		    		i: images.muralla,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Coliseo Romano",
		    			"Glaciar Perito Moreno",
		    			"Cañon del Colorado",
		    			"Monasterio San Lorenzo"
		    		],
		    		correct: "tunes",
		    		i: images.coliseo,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Taj Mahal",
		    			"Angel de la Independencia",
		    			"Mezquita de Cordoba",
		    			"Cibeles"
		    		],
		    		correct: "tunes",
		    		i: images.taj_majal,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Esfinge",
		    			"Jardines de Babilonia",
		    			"Centro Cultural Tijuana",
		    			"Stonehenge"
		    		],
		    		correct: "tunes",
		    		i: images.esfinge,
		    		correctIndex: 0
		    	},
		    	{
		    		options: [
		    			"Cristo Redentor",
		    			"Partenon",
		    			"Castillo de Edo",
		    			"Castillo de la cascada"
		    		],
		    		correct: "tunes",
		    		i: images.cristo,
		    		correctIndex: 0
		    	}
		    ];
		    
		    

		    

		    /*
		     * Add onto screen
		     *
		     */
		    
		    shapesLayer.add(scoreText);
		     
		    shapesLayer.add(option1);
		    shapesLayer.add(option2);
		    shapesLayer.add(option3);
		    shapesLayer.add(option4);

		    shapesLayer.add(option1Text);
		    shapesLayer.add(option2Text);
		    shapesLayer.add(option3Text);
		    shapesLayer.add(option4Text);

/* 			shapesLayer.add(casa); */

		    shapesLayer.add(marco);
		    shapesLayer.add(funny);
		    //shapesLayer.add(dinosaur);
/* 		    shapesLayer.add(dinosaurShape); */

		    shapesLayer.add(clock);
		    shapesLayer.add(timerText);
		    shapesLayer.add(streakText);
		    
		    shapesLayer.add(yeti);
		    
		    yeti.start();

		    backgroundLayer.add(bg);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
/* 			shapesLayer.add(totem); */
/* 			shapesLayer.add(totem2); */
			
			var currentCountryLevel = 0;
			var currentStreak = 0;

		    option1Text.setText(levels[currentCountryLevel].options[0]);
		    option2Text.setText(levels[currentCountryLevel].options[1]);
		    option3Text.setText(levels[currentCountryLevel].options[2]);
		    option4Text.setText(levels[currentCountryLevel].options[3]);

		    funny.setImage(levels[currentCountryLevel].i);
		    
		    function validateAnswer(answer) {

		    	if (answer == levels[currentCountryLevel].correctIndex)
		    		return true;
		    	return false;		    		
		    }

		    function displayNextCountries() {
			    
			    // Calc next position
			    
			    // var x = Math.random() * 1000 % (marco.getWidth() - dinosaur.getWidth()) + marco.getX() ;
			    // var y = Math.random() * 1000 % (marco.getHeight() - dinosaur.getWidth()) + marco.getY();
			    
			    //dinosaur.setX(x);
			    //dinosaur.setY(y);
			    
			    // Update score
		    	scoreText.setText(score);
		    	
		    	// Goto next step
			    currentCountryLevel++;
			    
			    
			    // Update labels
			    option1Text.setText(levels[currentCountryLevel].options[0]);
			    option2Text.setText(levels[currentCountryLevel].options[1]);
			    option3Text.setText(levels[currentCountryLevel].options[2]);
			    option4Text.setText(levels[currentCountryLevel].options[3]);
			    
			    // Update streak
			    streakText.setText('x'+currentStreak);
			    
			    
				funny.setImage(levels[currentCountryLevel].i);

			    funny.setOpacity(0);
			    var bgTween = new Kinetic.Tween({
			    	node: funny,
			    	opacity: 1,
			    	duration: 8,
			    	easing:		Kinetic.Easings.EaseIn,
			    	onFinish: function () {
	
			    	}
			    });
	
	/* 		    tween.play(); */
			    bgTween.play();
			    
		    }
		    
		    /*
		     * Tweening
		     *
		     */

/* 		    funny.setScale(.5) */
		    var bgTween = new Kinetic.Tween({
		    	node: funny,
		    	opacity: 1,
		    	duration: 8,
		    	easing:		Kinetic.Easings.EaseIn,
		    	onFinish: function () {

		    	}
		    });

/* 		    tween.play(); */
		    bgTween.play();

		    /*
		     * Events
		     *
		     */
		     
		    function optionHandler(index) {
			    console.log(validateAnswer(index));
			    sounds.swoosh.play();
		    	if (validateAnswer(index)) {
		    		yeti.setAnimation('hooray');
		    		yeti.afterFrame(19, function () {
			    		yeti.setAnimation('idle');
		    		});
			    	score += Math.round(150 * Math.random() * 10);
			    	currentStreak++;
		    	} else {
			    	currentStreak = 0;
		    	}
		    	displayNextCountries();
		    }
		     
		    option1.on("mousedown touchstart", function () {
		    	optionHandler(0);
		    });
		    
		    option1Text.on("mousedown touchstart", function () {
		    	optionHandler(0);
		    });

		    option2.on("mousedown touchstart", function () {
		    	optionHandler(1);
		    });
		    
		    option2Text.on("mousedown touchstart", function () {
		    	optionHandler(1);
		    });

		    option3.on("mousedown touchstart", function () {
		    	optionHandler(2);
		    });
		    
		    option3Text.on("mousedown touchstart", function () {
		    	optionHandler(2);
		    });

		    option4.on("mousedown touchstart", function () {
		    	optionHandler(3);
		    });
		    
		    option4Text.on("mousedown touchstart", function () {
		    	optionHandler(3);
		    });


		    /*
		     * Clock handling
		     *
		     */

		    function timerDidStop() {
		    	target = LEVEL.LEVEL5;
			    transitionFadeOut(LEVEL.LEVELLOADER);
		    }

		    function tick() {
		    	timerText.setText(parseInt(timerText.getText()) - 1);
		    	sounds.tick.play();
		    	if (timerText.getText() == "0") {
		    		clearInterval(interval);
		    		timerDidStop();
		    	}
		    }

		    var interval = setInterval(tick, timers[3]);
			
		}, 
		
		/*
		 *
		 * END of Level 4
		 *
		 * Beginning of Level 5
		 *
		 * GPS
		 *
		 */
		
		gotoLevel5: function () {
			
			console.log("Level 5");
			
			shapesLayer.add(winPlaceholder);
			shapesLayer.add(winText);
			
			var bg = new Kinetic.Image({
			   image: images.etapa5,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight,
			   filter: Kinetic.Filters.Colorize
		    });
		    
		    bg.setFilterColorizeColor([10,30,30]);
		    		    
		    var clock = new Kinetic.Image({
		    	image: images.clock,
		    	x: screenWidth - 160,
		    	y: 20
		    });
		    
		    var marco = new Kinetic.Image({
			   x: screenWidth / 10,
			   y: (clock.getY() + clock.getHeight()),
			   image: images.marco,
			   height: screenHeight * 0.7,
			   width: screenWidth * 0.6
		    });		    
		    
		    var timerText = new Kinetic.Text({
		    	x: clock.getX(),
		    	y: clock.getY() + 35,
		    	text: '60',
		    	fontSize: 56,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });

		    var option1 = new Kinetic.Image({
		    	x: marco.getX() + marco.getWidth() + 20 * screenWidthFactor,
		    	y: screenHeight / 2 - screenHeight * 0.2,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option2 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option1.getY() + option1.getHeight() + 10 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option3 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option2.getY() + option2.getHeight() + 10 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option4 = new Kinetic.Image({
		    	x: option1.getX(),
		    	y: option3.getY() + option3.getHeight() + 10 * screenHeightFactor,
		    	image: images.button,
		    	width: 180 * screenWidthFactor,
		    	height: 50 * screenHeightFactor
		    });

		    var option1Text = new Kinetic.Text({
		    	x: option1.getX(),
		    	y: option1.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option1.getWidth()
		    });

		    var option2Text = new Kinetic.Text({
		    	x: option2.getX(),
		    	y: option2.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option2.getWidth()
		    });

		    var option3Text = new Kinetic.Text({
		    	x: option3.getX(),
		    	y: option3.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option3.getWidth()
		    });

		    var option4Text = new Kinetic.Text({
		    	x: option4.getX(),
		    	y: option4.getY() + 10 * screenHeightFactor,
		    	text: "Sample",
		    	fontSize: 32,
		    	fontFamily: "Chicken Butt",
		    	fill: "#fff",
		    	align: "center",
		    	width: option4.getWidth()
		    });

		    var dinosaur = new Kinetic.Image({
		    	x: marco.getX() + 50,
		    	y: marco.getY() + 50,
		    	image: images.dinosaur
		    });
		    
		    dinosaur.setScale(.5);

		    var dinosaurShape = new Kinetic.RegularPolygon({
		    	x: stage.getWidth() / 2,
		    	y: stage.getHeight() / 2,
		    	sides: 4,
		    	radius: dinosaur.getWidth(),
		    	fillPatternImage: images.dinosaur,
		    	stroke: 'black',
		    	strokeWidth: 0,
		    	rotation: Math.PI / 4
		    });

		    var funny = new Kinetic.Image({
		    	x: marco.getX() + 5,
		    	y: marco.getY() + 5,
		    	image: images.funny,
		    	width: marco.getWidth() - 10,
		    	height: marco.getHeight() - 10,
		    	opacity: 0
		    });
		    
		    funny.setOpacity(0);
		    
		    var scoreText = new Kinetic.Text({
		    	x: 50,
		    	y: 20,
		    	text: score,
		    	fontSize: 56,
		    	fontFamily: "Chicken Butt",
		    	fill: "#555"
		    });
		    
		    var streakText = new Kinetic.Text({
		    	x:	clock.getX(),
		    	y:	clock.getY() + 85,
		    	text: 'x0',
		    	fontSize: 20,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: clock.getWidth()
		    });
		    

		    /*
		     * Add onto screen
		     *
		     */
		    
		    shapesLayer.add(scoreText);
		     
		    shapesLayer.add(option1);
		    shapesLayer.add(option2);
		    shapesLayer.add(option3);
		    shapesLayer.add(option4);

		    shapesLayer.add(option1Text);
		    shapesLayer.add(option2Text);
		    shapesLayer.add(option3Text);
		    shapesLayer.add(option4Text);

		    shapesLayer.add(marco);
		    shapesLayer.add(funny);
		    //shapesLayer.add(dinosaur);
/* 		    shapesLayer.add(dinosaurShape); */

		    shapesLayer.add(clock);
		    shapesLayer.add(timerText);
		    shapesLayer.add(streakText);

		    backgroundLayer.add(bg);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
			
			var currentCountryLevel = 0;
			var currentStreak = 0;

		    option1Text.setText(level5Answers[currentCountryLevel].options[0]);
		    option2Text.setText(level5Answers[currentCountryLevel].options[1]);
		    option3Text.setText(level5Answers[currentCountryLevel].options[2]);
		    option4Text.setText(level5Answers[currentCountryLevel].options[3]);

		    funny.setImage(level5Answers[currentCountryLevel].i);
		    
		    function validateAnswer(answer) {

		    	if (answer == level5Answers[currentCountryLevel].correctIndex)
		    		return true;
		    	return false;		    		
		    }

		    function displayNextCountries() {
			    
			    // Calc next position
			    
			    var x = Math.random() * 1000 % (marco.getWidth() - dinosaur.getWidth()) + marco.getX() ;
			    var y = Math.random() * 1000 % (marco.getHeight() - dinosaur.getWidth()) + marco.getY();
			    
			    //dinosaur.setX(x);
			    //dinosaur.setY(y);
			    
			    // Update score
		    	scoreText.setText(score);
		    	
		    	// Goto next step
			    currentCountryLevel++;
			    
			    
			    // Update labels
			    option1Text.setText(level5Answers[currentCountryLevel].options[0]);
			    option2Text.setText(level5Answers[currentCountryLevel].options[1]);
			    option3Text.setText(level5Answers[currentCountryLevel].options[2]);
			    option4Text.setText(level5Answers[currentCountryLevel].options[3]);
			    
			    // Update streak
			    streakText.setText('x'+currentStreak);
			    
			    
				funny.setImage(level5Answers[currentCountryLevel].i);

			    funny.setOpacity(0);
			    var bgTween = new Kinetic.Tween({
			    	node: funny,
			    	opacity: 1,
			    	duration: 8,
			    	easing:		Kinetic.Easings.EaseIn,
			    	onFinish: function () {
	
			    	}
			    });
	
	/* 		    tween.play(); */
			    bgTween.play();
			    
		    }
		    
		    /*
		     * Tweening
		     *
		     */

/* 		    funny.setScale(.5) */
		    var bgTween = new Kinetic.Tween({
		    	node: funny,
		    	opacity: 1,
		    	duration: 8,
		    	easing:		Kinetic.Easings.EaseIn,
		    	onFinish: function () {

		    	}
		    });

/* 		    tween.play(); */
		    bgTween.play();

		    /*
		     * Events
		     *
		     */
		     
		    function optionHandler(index) {
			    console.log(validateAnswer(index));
			    sounds.swoosh.play();
		    	if (validateAnswer(index)) {
		    		console.log(bgTween);
			    	score += Math.round(150 * Math.random() * 10);
			    	currentStreak++;
		    	} else {
			    	currentStreak = 0;
		    	}
		    	displayNextCountries();
		    }
		     
		    option1.on("mousedown touchstart", function () {
		    	optionHandler(0);
		    });
		    
		    option1Text.on("mousedown touchstart", function () {
		    	optionHandler(0);
		    });

		    option2.on("mousedown touchstart", function () {
		    	optionHandler(1);
		    });
		    
		    option2Text.on("mousedown touchstart", function () {
		    	optionHandler(1);
		    });

		    option3.on("mousedown touchstart", function () {
		    	optionHandler(2);
		    });
		    
		    option3Text.on("mousedown touchstart", function () {
		    	optionHandler(2);
		    });

		    option4.on("mousedown touchstart", function () {
		    	optionHandler(3);
		    });
		    
		    option4Text.on("mousedown touchstart", function () {
		    	optionHandler(3);
		    });


		    /*
		     * Clock handling
		     *
		     */

		    function timerDidStop() {
			    transitionFadeOut(LEVEL.SCORE);
		    }

		    function tick() {
		    	timerText.setText(parseInt(timerText.getText()) - 1);
		    	sounds.tick.play();
		    	if (timerText.getText() == "0") {
		    		clearInterval(interval);
		    		timerDidStop();
		    	}
		    }

		    var interval = setInterval(tick, timers[4]);
			
		},
		
		/*
		 *
		 * END of Level 5
		 *
		 * Beginning of Results
		 *
		 */
		
		gotoResults: function () {
			
			backgroundLayer.add(animatedBG);
			
			animatedBG.start();
			
			var d = 5000;
	    	var c = score;
	    	var b = 0;
	    	var t = 0;
	    	
	    	var salir = new Kinetic.Image({
		        image: images.salir,
		        width: images.salir.width * 0.5 * screenWidthFactor,
		        height: images.salir.height * 0.5 * screenHeightFactor,
		        x: screenWidth / 8,
		        y: screenHeight * 0.8,
		    });
		    
		    backgroundLayer.add(salir);
		    
/* 		    salir.setX(screenWidth / 2 - salir.getWidth() / 2 + 0); */
	    	
	    	salir.on('mouseover touchstart', function () {
				salir.setImage(images.salir_alt);
			});
			
			salir.on('mouseout touchstart', function () {
				salir.setImage(images.salir);
			});
			
			salir.on('mousedown touchstart', function () {
				transitionFadeOut(0);
			});
	    	
	    	var countdown = new Kinetic.Text({
			    x: screenWidth / 2 - 199,
			    y: screenHeight / 2,
			    fontSize: 144,
			    fontFamily: 'Chicken Butt',
			    fill: '#555',
			    align: 'center',
			    width: 398,
			    text: 3
		    });
		    
		    backgroundLayer.add(countdown);
			
			var next = new Kinetic.Image({
			   x: screenWidth / 2 - 199,
			   y: 70,
			   image: images.marco,
			   height: 100
		    });
		    
		    var nextText = new Kinetic.Text({
			    x: screenWidth / 2 - 199,
			    y: 90,
			    fontSize: 56,
			    fontFamily: 'Chicken Butt',
			    fill: '#555',
			    align: 'center',
			    width: 398,
			    text: user_name
		    });
		    
		    backgroundLayer.add(next);
		    backgroundLayer.add(nextText);
			
			var anim = new Kinetic.Animation( function(frame) {
		    	
		    	/*
		    		@t is the current time (or position) of the tween. This can be seconds or frames, steps, seconds, ms, whatever – as long as the unit is the same as is used for the total time [3].
					@b is the beginning value of the property.
					@c is the change between the beginning and destination value of the property.
					@d is the total time of the tween.
		    	 */
		    	
		    	t = frame.time;
		    	
		    	var step = Math.floor(c*(t/=d)*t*t*t + b);
		    	
		    	countdown.setText(step);
		    	backgroundLayer.draw();
		    	
		    	if (step > score) {
		    		countdown.setText(score);
			    	backgroundLayer.draw();
			    	anim.stop();
		    	}
		    	
		    }, backgroundLayer);
		    
		    anim.start();
		    
		    
		    $.ajax({
			    url: '/user/' + user_name + '/addHighscore',
			    method: 'POST',
			    data: {
				    highscore: score
			    },
			    success: function (data) {
				    console.log(data);
			    }
		    });
		},
		
		gotoOptions: function () {
			
			var bg = new Kinetic.Image({
			   image: images.opciones_bg,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight,
/* 			   filter: Kinetic.Filters.Colorize */
		    });
		    
		    var salir = new Kinetic.Image({
		        image: images.salir,
		        width: images.salir.width * 0.5 * screenWidthFactor,
		        height: images.salir.height * 0.5 * screenHeightFactor,
		        x: screenWidth / 10,
		        y: screenHeight * 0.8,
		    });
		    
		    shapesLayer.add(salir);
		    
		    backgroundLayer.add(bg);
		    stage.add(backgroundLayer);
			
			var sonido = new Kinetic.Image({
				image: sound_on ? images.sonido_on : images.sonido_off,
				x: 160 * screenWidthFactor,
				y: 97 * screenHeightFactor,
				on: sound_on
			});
			
			
			var username = new Kinetic.Text({
		    	x:	540 * screenWidthFactor,
		    	y:	sonido.getY(),
		    	text: user_name,
		    	fontSize: 40,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555'
		    });
			
			shapesLayer.add(username);
			shapesLayer.add(sonido);
			stage.add(shapesLayer);
			
			sonido.on('mouseover touchstart', function () {
				sonido.setImage( sonido.on ? images.sonido_off : images.sonido_on);
			});
			
			sonido.on('mouseout touchstart', function () {
				sonido.setImage(sonido.on ? images.sonido_on : images.sonido_off);
			});
			
			sonido.on('mousedown touchstart', function () {
				if (sonido.on) {
					sonido.setImage(images.sonido_off);
					sonido.on = false;
					sound_on = false;
					song.pause();
				} else {
					sonido.setImage(images.sonido_on);
					sonido.on = true;
					sound_on = true;
					song.play();
				}
			});
			
			username.on('mousedown touchstart', function(evt) {
				this.setText(prompt('Nombre de Usuario:'));
				shapesLayer.draw(); //redraw the layer containing the textfield
				user_name = this.getText();
			});
			
			salir.on('mouseover touchstart', function () {
				salir.setImage(images.salir_alt);
			});
			
			salir.on('mouseout touchstart', function () {
				salir.setImage(images.salir);
			});
			
			salir.on('mousedown touchstart', function () {
				transitionFadeOut(0);
			});
			
		},
		
		gotoScores: function () {
			
			console.log("Highscores");
			
			var bg = new Kinetic.Image({
			   image: images.etapa3,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight,
/* 			   filter: Kinetic.Filters.Colorize */
		    });
		    
/* 		    bg.setFilterColorizeColor([10,15,10]); */
		    
		    var salir = new Kinetic.Image({
		        image: images.salir,
/* 		        width: images.salir.width * 0.5 * screenWidthFactor, */
/* 		        height: images.salir.height * 0.5 * screenHeightFactor, */
		        x: screenWidth / 10,
		        y: screenHeight * 0.8,
		    });
		    
		    salir.setWidth(salir.getWidth() / 2 * screenWidthFactor);
		    salir.setHeight(salir.getHeight() / 2 * screenHeightFactor);
		    
		    shapesLayer.add(salir);
		    
/* 		    salir.setX(screenWidth / 2 - salir.getWidth() / 2 + 0); */
	    	
	    	salir.on('mouseover touchstart', function () {
				salir.setImage(images.salir_alt);
			});
			
			salir.on('mouseout touchstart', function () {
				salir.setImage(images.salir);
			});
			
			salir.on('mousedown touchstart', function () {
				transitionFadeOut(0);
			});
		    
		    var next = new Kinetic.Image({
			   x: screenWidth / 2 - 199,
			   y: 70,
			   image: images.marco,
			   height: 100
		    });
		    
		    var nextText = new Kinetic.Text({
			    x: screenWidth / 2 - 199,
			    y: 90,
			    fontSize: 56,
			    fontFamily: 'Chicken Butt',
			    fill: '#555',
			    align: 'center',
			    width: 398,
			    text: "Top 10"
		    });
		    
		    /*
		     * Add onto screen
		     *
		     */

		    shapesLayer.add(next);
		    shapesLayer.add(nextText);

		    backgroundLayer.add(bg);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
		    
		    $.ajax({
			    url: '/highscores',
			    success: function (data) {
				    
				    console.log(data);
				    $.each(data, function (i,user) {
				    	console.log(user.username + ' ' + user.maxHighscore);
				    	
				    	console.log(i)
				    	
				    	var name = new Kinetic.Text({
						    x: screenWidth / 2 - 400 * screenHeightFactor,
						    y: i * screenHeightFactor * 40 + 130 * screenHeightFactor,
						    fontSize: 56,
						    fontFamily: 'Chicken Butt',
						    fill: '#555',
						    align: 'right',
						    width: 398,
						    text: user.username
					    });
					    
					    var score = new Kinetic.Text({
						    x: screenWidth / 2 + 199 * screenHeightFactor,
						    y: i * screenHeightFactor * 40 + 130 * screenHeightFactor,
						    fontSize: 56,
						    fontFamily: 'Chicken Butt',
						    fill: '#555',
						    align: 'left',
						    width: 398,
						    text: user.maxHighscore
					    });
					    
					    shapesLayer.add(name);
					    shapesLayer.add(score);
				    	
				    });
			    }
		    })
			
		},
		
		gotoCredits: function () {
			
			var bg = new Kinetic.Image({
			   image: images.stage1_bg,
			   x: 0,
			   y: 0,
			   width: screenWidth,
			   height: screenHeight,
			   filter: Kinetic.Filters.Colorize
		    });
		    
		    bg.setFilterColorizeColor([7,5,2]);
		    
		    var salir = new Kinetic.Image({
		        image: images.salir,
		        width: images.salir.width * 0.5 * screenWidthFactor,
		        height: images.salir.height * 0.5 * screenHeightFactor,
		        x: screenWidth / 10,
		        y: screenHeight * 0.8,
		    });
		    
		    shapesLayer.add(salir);
		    
/* 		    salir.setX(screenWidth / 2 - salir.getWidth() / 2 + 0); */
	    	
	    	salir.on('mouseover touchstart', function () {
				salir.setImage(images.salir_alt);
			});
			
			salir.on('mouseout touchstart', function () {
				salir.setImage(images.salir);
			});
			
			salir.on('mousedown touchstart', function () {
				transitionFadeOut(0);
			});
		    
		    var credits = new Kinetic.Text({
		    	x: 0,
		    	y: bg.getHeight(),
		    	text: 'Programación: Martin Goffan \n Diseño: Nicolas Miranda y Francisco Kraefft \n',
		    	fontSize: 60 * screenHeightFactor,
		    	fontFamily: 'Chicken Butt',
		    	fill: '#555',
		    	align: 'center',
		    	width: bg.getWidth()
		    });
		    
		    backgroundLayer.add(bg);
		    
		    shapesLayer.add(credits);
		    
		    stage.add(backgroundLayer);
		    stage.add(shapesLayer);
		    
			var anim = new Kinetic.Animation(function(frame) {
				
				var amp = screenHeight*screenHeightFactor;
				
				credits.setY(-Math.tan((frame.time % 12300) * Math.PI/50000) * amp + amp / 2 + credits.getHeight());
			
			}, shapesLayer);
			
			anim.start();
		},
		
		
		/**
		  *
		  *
		  * Loaders
		  *
		  **/
		
		/**
		  *
		  * Level 1
		  *
		  */
		
		gotoLevelLoader: function (destination) {
			
			backgroundLayer.add(animatedBG);
			
			animatedBG.start();
			
			var countdown = new Kinetic.Text({
			    x: screenWidth / 2 - 199,
			    y: screenHeight / 2 - 100,
			    fontSize: 56,
			    fontFamily: 'Chicken Butt',
			    fill: '#555',
			    align: 'center',
			    width: 398,
			    text: 3
		    });
		    
		    backgroundLayer.add(countdown);
		    
		    var anim = new Kinetic.Animation( function(frame) {
		    	
		    	if ( frame.time < 1000) {
			    	
			    	countdown.setFontSize(countdown.getFontSize() + frame.timeDiff);
			    	
		    	}
		    	else {
		    		countdown.setFontSize(56);
					
					countdown.setText(parseInt(countdown.getText()) - 1);
					
					if (countdown.getText() == '0') {
						anim.stop();
						countdown.remove();
						transitionFadeOut(destination);
					}
					
					frame.time = 0;
					
			    }
		    }, backgroundLayer);
		    
		    anim.start();
			
		}
		
		
	}
})(Game || {});

function initStage() {
	console.log("init");
	console.log(level5Answers);
    stage = new Kinetic.Stage({
        container: 'container',
        width: screenWidth,
        height: screenHeight
    });
    
    $('#progress').remove();
    
    animatedBG.stop();
    
    loadingLayer.removeChildren();
    
    shapesLayer 		= new Kinetic.Layer();
	backgroundLayer		= new Kinetic.Layer();
	polysLayer			= new Kinetic.Layer();
    
    Game.transitionFadeOut(0);
}

/* var animations = {idle: [{x: 2,y: 2,width: 70,height: 119}, {x: 71,y: 2,width: 74,height: 119}, {x: 146,y: 2,width: 81,height: 119}, {x: 226,y: 2,width: 76,height: 119}],punch: [{x: 2,y: 138,width: 74,height: 122}, {x: 76,y: 138,width: 84,height: 122}, {x: 346,y: 138,width: 120,height: 122}]}; */

stage = new Kinetic.Stage({
    container: 'container',
    width: screenWidth,
    height: screenHeight
});

var loadingLayer = new Kinetic.Layer();

var animatedBackgroundImage = new Image();

var animatedBG;

animatedBackgroundImage.onload = function () {
	
	animatedBG = new Kinetic.Sprite({
	   image: animatedBackgroundImage,
	   x: 0,
	   y: 0,
	   width: screenWidth,
	   height: screenHeight,
	   animations: ANIMATIONS.LOAD.FONDO,
	   animation: 'idle',
	   frameRate: 11,
	   index: 0,
	   scale: [screenWidth / 600, screenHeight / 600]
    });
    
    loadingLayer.add(animatedBG);
    
    animatedBG.start();
	
	loadingLayer.add(loadingRectText);
	loadingLayer.add(loadingDialogText);
	
	loadAnim();
}

animatedBackgroundImage.src = '../assets/animeted_background.png';

var loadingRectText = new Kinetic.Text({
	x: 0,
	y: screenHeight / 2 + 30 * screenHeightFactor,
	text: '0',
	fontSize: 56,
	fontFamily: 'Chicken Butt',
	fill: '#555',
	align: 'center',
	width: screenWidth
});

var loadingDialogText = new Kinetic.Text({
	x: 0,
	y: loadingRectText.getY() + 30 * screenHeightFactor,
	text: 'Cargando GPS',
	fontSize: 48,
	fontFamily: 'Chicken Butt',
	fill: '#555',
	align: 'center',
	width: screenWidth
});

var mundoAnimations = ANIMATIONS.LOAD.MUNDO;

var mundoImage = new Image();

function loadAnim() {

	mundoImage.onload = function () {
	
		var mundoKineticImage = new Kinetic.Sprite({
		    x: screenWidth / 2 - 125,
		    y: screenHeight / 2 - 125 - 75 * screenHeightFactor,
		    image: mundoImage,
		    animation: 'idle',
		    animations: ANIMATIONS.LOAD.MUNDO,
		    frameRate: 12,
		    index: 0
		});
		
		loadingLayer.add(mundoKineticImage);
	
		mundoKineticImage.start();
		
		var dataLoader = new DataLoader(
		
			sources,
			sourceFlags,
			sourceSounds,
			function (progress) {
				loadingRectText.setText(progress + '%');
				loadingRectText.getLayer().draw();
			},
			function (msg) {
				loadingDialogText.setText(msg);
				loadingDialogText.getLayer().draw();
			},
			function () {
				winPlaceholder.setImage(images.placeholder);
				winPlaceholder.setWidth(winPlaceholder.getWidth() * screenWidthFactor);
				winPlaceholder.setHeight(winPlaceholder.getHeight() * screenHeightFactor);
				winPlaceholder.setY(-winPlaceholder.getHeight());
				winPlaceholder.setX(screenWidth / 2 - winPlaceholder.getWidth() / 2);
				
				winText.setY(winPlaceholder.getY() + 30 * screenHeightFactor);
				
/* 				shapesLayer.draw(); */
				initStage();
			},
			sourceMonuments);
		dataLoader.load();
		
		
		
	/* 	load(sources, sourceFlags, sourceSounds); */
		
	}
	
	mundoImage.src = '../assets/mundo.png';
}


stage.add(loadingLayer);