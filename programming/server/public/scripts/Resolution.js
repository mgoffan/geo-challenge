var Resolution = (function () {
	
	var   screenWidth = window.innerWidth
	    , screenHeight = window.innerHeight
	    , screenWidthFactor = screenWidth / 760
	    , screenHeightFactor = screenHeight / 600;
	
	
	return {
		screenWidth: screenWidth,
		screenHeight: screenHeight,
		screenWidthFactor: screenWidthFactor,
		screenHeightFactor: screenHeightFactor
	};
	
})(Resolution || {});