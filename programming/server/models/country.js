var mongoose = require("mongoose");

/*
 *  Build Model
 *
 */
 
var PaisSchema = mongoose.Schema({

	nombre: {
		type: String,
		required: true,	
	},
	x: {
		type: Number,
		default: 0
	},
	y: {
		type: Number,
		default: 0
	},
	dificultad: {
		type: Number,
		default: 1
	},
	capital: {
		type: String,
		required: true
	},
	ciudades: [{
		nombre: {
			type: String
		},
		x: {
			type: Number,
			default: 0
		},
		y: {
			type: Number,
			default: 0
		}
	}],
	relevancia: { // Tiene monumentos importantes
		type: Boolean,
		default: false
	},
	monumentos: [{
		nombre: {
			type: String
		},
		x: {
			type: Number,
			default: 0
		},
		y: {
			type: Number,
			default: 0
		},
		url: {
			type: String
		}
	}],
	continente: {
		type: String,
		required: true
	}
});


var Country = mongoose.model('Country', PaisSchema);

module.exports = Country;