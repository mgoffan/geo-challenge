var mongoose = require("mongoose");

var UserSchema = mongoose.Schema({
    //name: { type: String, required: true },
    //surname: { type: String, required: true },
    username: { type: String, required: true, index: { unique: true}},
    //email: { type: String, required: true },
    //password: { type: String, required: true },
    highscores: [{
		type: Number,
		required: true
	}],
	maxHighscore: {
		type: Number,
		required: true
	}
});

var User = mongoose.model("User", UserSchema);

module.exports = User;